#include "stdafx.h"
#include "link.h"

link ::link()
{
	corsim_org = 0;
	corsim_dest =0;
	OppositeLink = 0;
}
link::~link(){}

void link::details(int id, int type, int org, int dest, float length, float speed, int capacity, int lanes)
{
	vista_id = id;
	vista_type = type;
	vista_org = org;
	vista_dest = dest;
	vista_length = length;
	vista_speed = speed;
	vista_capacity= capacity;
	vista_lanes = lanes;
}

void link::inputVistaID(int id)
{
	vista_id = id;
}

void link::inputCorsimUp(int up)
{
	corsim_org = up;
	
}

void link::inputCorsimDn(int dn)
{

	corsim_dest = dn;	
}

void link::inputCorsimLength(float length)
{
	corsim_length = length;
}

int link::returnVistaID()
{
	return vista_id;
}

int link::returnVistaType()
{
	return vista_type;
}

int link::returnVistaOrg()
{
	return vista_org;
}

int link::returnVistaDest()
{
	return vista_dest;
}

void link::CosimNodes(int up, int dn)
{
	corsim_org = up;
	corsim_dest = dn;

}

int link:: returnCorsimUp()
{
	return corsim_org;
}

int link::returnCorsimDn()
{
	return corsim_dest;
}

float link::returnVistaLength()
{
	return vista_length;
}

float link::returnVistaSpeed()
{
	return vista_speed;
}

int link::returnCorsimSpeed()
{
	corsim_speed = int (vista_speed * 60); 
	return corsim_speed;
}

int link::returnVistaCapacity()
{
	return vista_capacity;
}

int link::returnVistaLanes()
{
	return vista_lanes;
}

void link::orientation(vector <int> orient)
{
	Orient = orient;  //vector downnode <--upnode
	corsim_length =sqrt( pow(float (Orient[0]), 2) + pow(float(Orient[1]), 2));
	if (corsim_length == 0) corsim_length = vista_length;
	float up_cos = float ((Orient[0]/corsim_length));
	float up_sin = float ((Orient[1]/corsim_length));
	float angle_cos = acos(up_cos)*180/3.14159265;
	float angle_sin = asin(up_sin)*180/3.14159265;
	if (vista_id== 18516)
		int abc = 123;	
	
	if (Orient[0] == 0 ||Orient[1] == 0)
	{
		if (Orient[0] == 0 && Orient[1] == 0) angle = -1;
		else if (Orient[1] == 0)
		{
			if (Orient[0] > 0) angle = 0;
			else angle = 180;
		}
		else if (Orient[0] == 0)
		{
			if (Orient[1] > 0) angle = 90;
			else angle = 270;
		}
	}
	else if (Orient[0] > 0 && Orient[1] > 0)angle = int ((abs(angle_cos) + abs(angle_sin))/2);
	else if (Orient[0] < 0 && Orient[1] > 0)angle = angle_cos;
	else if (Orient[0] < 0 && Orient[1] < 0)angle = 360 - angle_cos; 
	else if (Orient[0] > 0 && Orient[1] < 0)angle = 360 - angle_cos;

	int finalanlge = angle;
}

vector <int> link:: returnOrient()
{
	return Orient;
}

float link ::returnCORSIMLength()
{
	return corsim_length;
}

int link :: returnAngle()
{
	return angle;
}

void link::Downstreams(int size, vector <vector <int>> dnlinks)  //0=link id, 1 = angle, 2 = corsim downnode 3 = Orientation (1=left, 2=throug, 3 = right),
{
	/*
		37 - 40 Downstream node receiving left-turning traffic (1-6999,7000-7999,8000-8999 Node ID)
		41 - 44 Downstream node receiving through traffic (1-6999,7000-7999,8000-8999 Node ID)
		45 - 48 Downstream node receiving right-turning traffic (1-6999,7000-7999,8000-8999 Node ID)
		49 - 52 Downstream node receiving diagonal traffic (1-6999,7000-7999,8000-8999 Node ID)
		maximum size is 4!!
		turn[4] 0=left, 1=through, 2=right, 3=diag (no specific which diag) try not to use diag....
	*/
	if (vista_id== 18516)
		int abc = 123;


	dn_corsim_nodes.resize(5);
	dn_vista_links.resize(5);
	std::fill(dn_corsim_nodes.begin(),dn_corsim_nodes.end(), 0 );
	std::fill(dn_vista_links.begin(),dn_vista_links.end(), 0 );

	NumDnLinks = size;
	if (size == 1) 
	{
		dn_corsim_nodes[1] = dnlinks[0][2];  //only one downstream links
		dn_vista_links[1] = dnlinks[0][0];  //only one downstream links
	}
	else if (size == 2) 
	{
		for (int i = 0; i < size; i ++)
		{
			if (dnlinks[i][3] == 1)
			{
				dn_corsim_nodes[0] = dnlinks[i][2];
				dn_vista_links[0] = dnlinks[i][0];  	

			}
			if (dnlinks[i][3] == 2)
			{
				dn_corsim_nodes[1] = dnlinks[i][2];
				dn_vista_links[1] = dnlinks[i][0];  
			}
			if (dnlinks[i][3] == 3)
			{
				dn_corsim_nodes[2] = dnlinks[i][2];
				dn_vista_links[2] = dnlinks[i][0]; 
			}

/*
			if (dnlinks[i][3] == 1)
			{
				if (i == 0)
				{
					if ((dnlinks[i][1] < 120 || dnlinks[i][1] > 240)  && (min (360-dnlinks[i][1],dnlinks[i][1])< min(dnlinks[i+1][1], 360-dnlinks[i+1][1]) ))
					{
						dn_corsim_nodes[1] = dnlinks[i][2];
						dn_vista_links[1] = dnlinks[i][0];  						
					}	
					else
					{
						dn_corsim_nodes[0] = dnlinks[i][2];
						dn_vista_links[0] = dnlinks[i][0];  
					}
				}
				if (i == 1)
				{
					if ((dnlinks[i][1] < 120 || dnlinks[i][1] > 240)  && (min (360-dnlinks[i][1],dnlinks[i][1])< min(dnlinks[i-1][1], 360-dnlinks[i-1][1]) ))
					{
						dn_corsim_nodes[1] = dnlinks[i][2];
						dn_vista_links[1] = dnlinks[i][0];  						
					}	
					else
					{
						dn_corsim_nodes[0] = dnlinks[i][2];
						dn_vista_links[0] = dnlinks[i][0];  
					}
				}

			}
			if (dnlinks[i][3] == 2)
			{
					
				if (i == 0)
				{
					if ((dnlinks[i][1] < 120 || dnlinks[i][1] > 240)  && (min (360-dnlinks[i][1],dnlinks[i][1])< min(dnlinks[i+1][1], 360-dnlinks[i+1][1]) ))
					{
						dn_corsim_nodes[1] = dnlinks[i][2];
						dn_vista_links[1] = dnlinks[i][0];  						
					}	
					else
					{
						dn_corsim_nodes[2] = dnlinks[i][2];
						dn_vista_links[2] = dnlinks[i][0];  
					}
				}
				if (i == 1)
				{
					if ((dnlinks[i][1] < 120 || dnlinks[i][1] > 240)   && (min (360-dnlinks[i][1],dnlinks[i][1])< min(dnlinks[i-1][1], 360-dnlinks[i-1][1]) ))
					{
						dn_corsim_nodes[1] = dnlinks[i][2];
						dn_vista_links[1] = dnlinks[i][0];  						
					}	
					else
					{
						dn_corsim_nodes[2] = dnlinks[i][2];
						dn_vista_links[2] = dnlinks[i][0];  
					}
				}

			}*/
		}
	}
	else if (size == 3) 
	{
		for (int i = 0; i < size; i ++)
		{
			if (dnlinks[i][3] == 1)
			{
				dn_corsim_nodes[0] = dnlinks[i][2];
				dn_vista_links[0] = dnlinks[i][0];  	

			}
			if (dnlinks[i][3] == 2)
			{
				dn_corsim_nodes[1] = dnlinks[i][2];
				dn_vista_links[1] = dnlinks[i][0];  
			}
			if (dnlinks[i][3] == 3)
			{
				dn_corsim_nodes[2] = dnlinks[i][2];
				dn_vista_links[2] = dnlinks[i][0]; 
			}
		}
	}
	else if (size == 4) 
	{
		for (int i = 0; i < size; i ++)
		{
			if (dnlinks[i][3] == 1)
			{
				dn_corsim_nodes[0] = dnlinks[i][2];
				dn_vista_links[0] = dnlinks[i][0]; 
			}
			if (dnlinks[i][3] == 2)
			{
				dn_corsim_nodes[1] = dnlinks[i][2];
				dn_vista_links[1] = dnlinks[i][0]; 
			}
			if (dnlinks[i][3] == 3)
			{
				dn_corsim_nodes[2] = dnlinks[i][2];
				dn_vista_links[2] = dnlinks[i][0]; 

			}
			if (dnlinks[i][3] == 4)
			{
				dn_corsim_nodes[3] = dnlinks[i][2];
				dn_vista_links[3] = dnlinks[i][0]; 
			}
		}
	}
	else if (size == 5) 
	{
		for (int i = 0; i < size; i ++)
		{
			if (dnlinks[i][3] == 1)
			{
				dn_corsim_nodes[0] = dnlinks[i][2];
				dn_vista_links[0] = dnlinks[i][0]; 
			}
			if (dnlinks[i][3] == 2)
			{
				dn_corsim_nodes[1] = dnlinks[i][2];
				dn_vista_links[1] = dnlinks[i][0]; 
			}
			if (dnlinks[i][3] == 3)
			{
				dn_corsim_nodes[2] = dnlinks[i][2];
				dn_vista_links[2] = dnlinks[i][0]; 

			}
			if (dnlinks[i][3] == 4)
			{
				dn_corsim_nodes[3] = dnlinks[i][2];
				dn_vista_links[3] = dnlinks[i][0]; 
			}
			if (dnlinks[i][3] == 5)
			{
				dn_corsim_nodes[4] = dnlinks[i][2];
				dn_vista_links[4] = dnlinks[i][0]; 
			}

		}
	}

	/*
	else if (size == 3) 
	{
		turn[0] = dnlinks[0][0];
		turn[1] = dnlinks[1][0];
		turn[2] = dnlinks[2][0];
	}
	else if (size == 4) 
	{
		turn[0] = dnlinks[0][0];
		turn[1] = dnlinks[1][0];
		turn[2] = dnlinks[2][0];
		turn[3] = dnlinks[3][0];
	}
	*/
}

int link::returnNumDns()
{
	return NumDnLinks;
}

vector <int> link:: returnDownstreams()
{
	return dn_vista_links;
	
}

vector <int> link :: returnDownNodes()
{
	return dn_corsim_nodes;

}

void link::Twoways(int id)
{
	 OppositeLink = id;
}

int link::returnTwoways()
{
	return  OppositeLink;
}

void link::DnsVehs(vector <int> vehs)
{
	if (vista_id == 218533) 
		int helo = 123;
	dnsvehtimes.push_back(vehs);
}

vector <vector <int> > link::returnDnsVehs(int dns)
{
	vector <vector <int> > vehtimes;
	
	for (int i = 0; i < dnsvehtimes.size(); i ++)
	{
		if (dnsvehtimes[i][0] == dns)
		{
			vector <int> inf;
			inf.push_back(dnsvehtimes[i][1]);
			inf.push_back(dnsvehtimes[i][2]);
			vehtimes.push_back(inf);
		}
	}

	return vehtimes;
}

vector<vector <int>> link:: returnLinkFlow()
{
	return dnsvehtimes;
}

vector<vector <int>> link::returnVehs()
{
	return dnsvehtimes;
}

void link::newType(int type)
{
	vista_type = type;
}

void link::replaceLink(int linkid)
{
	replace_vista_id = linkid;
}

int link::returnReplaceLink()
{
	return replace_vista_id;
}

void link:: combineLinkFlow(vector<vector <int>> flow)
{
	dnsvehtimes.insert(dnsvehtimes.end(), flow.begin(), flow.end());
}
	/*
int link::downnode()
{
	return link_dest;
}

int link::downlk()
{
	return link_id;
}

int link :: FLAG()
{
	return link_flag;
}

// corsim//
void link::getLinks(int node1, int node2, int left, int thru, int right, int diag, int opos, int flag1, int flag2)  // type 11 can get the nodes' network
{
	Node1 = node1;
	Node2 = node2;
	Left = left;
	Thru = thru;
	Right = right;
	Diag = diag;
	Opos = opos;
	Flag1 = flag1;
	Flag2 = flag2;
}

int link::returnNode1()
{
	return Node1;
}

int link::returnNode2()
{
	return Node2;
}

int link::returnLeft()
{
	return Left;
}

int link::returnRight()
{
	return Right;
}

int link::returnThru()
{
	return Thru;
}

int link::returnDiag()
{
	return Diag;
}

int link::returnOpos()
{
	return Opos;
}

int link::returnFlag1()
{
	return Flag1;
}

int link::returnFlag2()
{
	return Flag2;
}

void link ::subLinks(int node1, int node2)
{
	Node1 = node1;
	Node2 = node2;
}

void link ::SubNetwork(int node1, int node2, int left, int thru, int right, int diag)
{
	Node1 = node1;
	Node2 = node2;
	Left = left;
	Thru = thru;
	Right = right;
	Diag = diag;
}


void link ::SignalControl(int node2, int ap1, int ap2, int ap3, int ap4, int ap5)
{

	int link[5];
	link[0] = Left;
	link[1] = Thru;
	link[2] = Right;
	link[3] = Diag;
	link[4] = Node1;

	apt[0] = ap1;
	apt[1] = ap2;
	apt[2] = ap3;
	apt[3] = ap4;
	apt[4] = ap5;
	
	for (int i = 0; i < 5; i ++)
	{
		int tag = 0;
		for (int j = 0; j < 5; j ++)
		{
			if (apt[i] == link[j]) 
			{
				tag = 1;	
				break;
			}
		}
		if (tag == 0) apt[i] = 0;
	}
}

int link ::returnAPT1()
{
	return apt[0];
}

int link ::returnAPT2()
{
	return apt[1];
}

int link ::returnAPT3()
{
	return apt[2];
}

int link ::returnAPT4()
{
	return apt[3];
}

int link ::returnAPT5()
{
	return apt[4];
}*/