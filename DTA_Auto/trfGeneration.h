#ifndef trfGeneration_H
#define trfGeneration_H

#include <windows.h>

#include <stdlib.h>
#include <crtdbg.h>
#include <string>

#include "cardtrf.h"
#include "link.h"
#include "node.h"
#include "path.h"
#include"signals.h"

#include <stdio.h>
#include <vector>
#include <string>
#include <math.h>

#include <numeric>
//#include "vista.h"

#include "fake_link.h"
#include "fake_node.h"
using namespace std;

class trfGeneration
{
	public:
		trfGeneration(void);
		~trfGeneration(void);

		void CorsimIdtfy(int node1, int node2);
		void getTRF();
		void Type195_node();
		void output();
		void getVISTAPath();
		void Type11_Vista_LinkDescription();
		void Type21_TurnMovement();
		void getLinkOrient();
		void getVolume();
		void UpstreamNodes();
		
		int Signals();
		void Offsets();
		void ReorganizePhases();

		int DefineCentroid();
		int CheckCentroid1();
		int CheckNetwork();
		void FakeLinkandNode();
		void ChangeFakeLinkNodeBack();
		void Type35_36();
		void Type36();
		void Type50();
		void Type51();
		std::vector<char> pTRF; //for sample trf
		std::vector<char> TRF_195; //for node position trf
		std::vector<char> TRF_11;
		std::vector<char> TRF_21;
		std::vector<char> TRF_35;
		std::vector<char> TRF_36;
		std::vector<char> TRF_50;
		std::vector<char> TRF_51;
		std::vector<char> TRF_170;
		std::vector<char> TRF_210;

		std::vector <node> vsta_ndlist;
		std::vector <node> :: iterator nd_pos;
		vector <node> :: iterator nd_pos2;
		

		deque <path> vsta_pathlist;
		deque <path> :: iterator path_pos;

		vector <link> linklist;
		vector <link> :: iterator lk_pos;
		vector <link> :: iterator lk_pos2;

		vector <signals> signallist;
		vector <signals>::iterator spos;

		vector <vector <int>> int_signallist; // 0 = vista id, 1 = corsim id ( 2 = node 1, 3=node 2 ...... all vista id)

		std::vector<vector <int>> vista_links; //1=id, 2=org, 3=dest 4 corsim_org 5 corsim_dest
		std::vector<vector <int>> vista_network; //the connections between links [number of links][number of links] = 1==> connect, 0 ==> no connect
		std::vector<vector <int>> vista_corsim_nodes;  // 0 = vista node 1 = corsim node 2= signal flag (0 no 1 yes)  //only use in the beginning, until the function of fake nodes 
		std::vector <int> total_corsim_nodes;
		vector <vector <int>> replace_links;  //the connectors which should be replaced. =>to add the link flow from the removed connectors to existed links

		std::vector<vector <int>> vista_connectors_nodes; //develop the connector database which includes vista upnode and downnodes

		vector <int> include_nodes;
		std:: vector <int> :: iterator it;
		std:: vector <int> :: iterator it2;


	//	int parking_id;
		int boundary_id;

		//VISTA
		void getVistaNodes();
		void getVistaNetwork();	
		void getVistaLinks();
		void DefineCentroids();

		void getNetworkInfo(vector<int>& stlk, int);
		void CRSM_RelatedLink();
		int crsm_node[2];

		void getSubnetwork();
		void getCardType();
		int getNode(int line);
		int DeleteNoSubNode(int node);
		void getNewCentroid();
		void inputNewCentroid();
		void GreenPhase();
		int Type11_RegionalLinks(int);
		int Type11_SubLinks(int );
		int Type21_SubLinks(int);
		int Type35_DeleteSignal(int);
		int Type36_DeleteNode(int);
		int Type50_DeleteNode(int);

		int Type35_IdentifySignal(int line);

		int Type195_SubNodes(int );

		vector <fake_node> fake_nodelist;
		vector <fake_link> fake_linklist;

		vector <vector <int>> fakenodes;  //0=original up id, 1= orignal downnode 2= fake up id 






		//20160320

		 int  Type11_GetCentroidList(int);


		int getNodePosition(int line);
		void VISTAIdtfy(int node1, int node2);


		deque <link> vsta_sublk;
		deque <link> vsta_temlk;
		







		int vsta_node[2];




		void CRSM_GetNodes();
		void IDTF(int, int);
		int CM_NODE;
		int VISTA_NODE;

		int VT_NODE;

		// delete out of subnetwork

};
#endif