#ifndef VISTA_H
#define VISTA_H

#pragma once

#include <iostream>
#include <string>


class vista{

public: 
	vista(void);
	~vista(void);

	void details(int, int, int, int);
	int ID();
	int ORG();
	int DEST();
	int FLAG();

	int link_id;
	int link_org;
	int link_dest;
	int link_flag;


	void downstream(int, int);
	int downlk();
	int downnode();

	void node_locate(int, float, float);

	float node_X();
	float node_Y();
	int node_ID();

	int node_id;
	float node_x;
	float node_y;


	int node_crsm;
	int node_vsta;
	void CSVT_node(int, int, int);
	int NODE_CRSM();
	int NODE_VSTA();
	int NODE_FLAG();
	int node_flag;

};
#endif