#ifndef FAKE_LINK_H
#define FAKE_LINK_H

#pragma once

#include <iostream>
#include <string>
#include <fstream>
using namespace std;
#pragma once
#include <deque>
#include <vector>


class fake_link{

public: 
	 fake_link(void);
	~ fake_link(void);

	int vista_id;

	int corsim_org;
	int corsim_dest;
	int corsim_speed;
	int corsim_capacity;
	int corsim_lanes;
	float corsim_length;
	int corsim_opst;

	
	void details(int id, int org, int dest,  int opst, int speed, int capacity, int lanes, int length);



	int returnVistaID();
	int returnCorsimUp();
	int returnCorsimDn();
	int returnCorsimSpeed();
	int returnCorsimCapacity();
	int returnCorsimLanes();
	float returnCorsimLength();
	int returnCorsimOpst();





};
#endif