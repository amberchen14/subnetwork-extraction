#include "stdafx.h"
#include "signals.h"

signals ::signals(){}
signals::~signals(){}

void signals:: input(int id, int phase, int nodeid, int timered, int timeyellow, int timegreen, CString cs,CString vs)
{
	Signal_ID = id;
	Phases = phase;
	Node_ID = nodeid;
	Red = timered;
	Green = timegreen;
	Yellow = timeyellow;
	LinkFrom = cs;
	LinkTo = vs;
	

	phase_order.push_back(Phases);
	
	vector <int> phasetime;
	phasetime.push_back(Green);
	phasetime.push_back(Yellow);
	phasetime.push_back(Red);
	phase_time.push_back(phasetime);

	InputLinkFrom(LinkFrom);
	InputLinkTo(LinkTo);


}

void signals::inputCorsimNode(int node)
{
	Corsim_node = node;

}

int signals::returnCorsimNode()
{
	return Corsim_node;
}

vector <int> signals::returnPhaseOrder()
{
	return phase_order;
}

int signals::returnNodeID()
{
	return Node_ID;
}

vector <vector <int>> signals::returnPhaseTime()
{
	return phase_time;
}


void signals::InputLinkFrom(CString & links_string)
{

	std::vector <char> char_linkfrom;
	std::vector <int> int_linkfrom;
	char_linkfrom.resize(links_string.GetLength());
	std::transform(links_string.GetString(),
					links_string.GetString() + links_string.GetLength(),
                   char_linkfrom.begin(),
                    [](TCHAR c) { return static_cast<char>(c); });

	int link_id = 0;
	for (int i = 0; i < char_linkfrom.size(); i++)
	{
		if (char_linkfrom[i] == '{') continue;
		else if (char_linkfrom[i]== ',' || char_linkfrom[i] == '}')
		{	
			int_linkfrom.push_back(link_id);
			link_id = 0;
		}
		else
		{
			link_id = link_id*10+int(char_linkfrom[i]) - '0';
		}
	}

	phase_linkfrom.push_back(int_linkfrom);
}

void signals::InputLinkTo(CString & links_string)
{
	std::vector <char> char_linkto;
	std::vector <int> int_linkto;

	char_linkto.resize(links_string.GetLength());
	std::transform(links_string.GetString(),
					links_string.GetString() + links_string.GetLength(),
                    char_linkto.begin(),
                    [](TCHAR c) { return static_cast<char>(c); });

	int link_id = 0;
	for (int i = 0; i < char_linkto.size(); i++)
	{
		if (char_linkto[i] == '{') continue;
		else if (char_linkto[i]== ',' || char_linkto[i] == '}')
		{	
			int_linkto.push_back(link_id);
			link_id = 0;
		}
		else
		{
			link_id = link_id*10+int(char_linkto[i]) - '0';
		}
	}
	phase_linkto.push_back(int_linkto);
}


vector <vector <int>> signals::returnLinkFrom() 
{
	return phase_linkfrom;
}

vector <vector <int>>  signals::returnLinkTo()
{
	return phase_linkto;
}

void signals:: InputNodeFrom(vector<int> nodes)
{
	node_from.push_back( nodes);

}

vector <vector <int>> signals::  returnNodeFrom()
{
	return node_from;
}

vector <vector <int>> signals::  returnNodeTo()
{
		return node_to;
}

void signals::InputNodeTo(vector<int> nodes)
{
	node_to.push_back( nodes);
}

void signals::RecordPhaseColor(vector<vector<int>> uplinks, vector<vector<int>> phasecolor)
{
	 UniqueUplinks = uplinks;
	 PhaseColor = phasecolor;	
}
vector<vector<int>> signals:: returnUniqueUplinks()
{
	return  UniqueUplinks;
}

vector<vector<int>> signals::returnPhaseColor()
{
	return PhaseColor;
}
