#ifndef PATH_H
#define PATH_H

#include <windows.h>

#include <stdlib.h>
#include <crtdbg.h>
#include <string>
#include <algorithm>    // std::transform

#include <stdio.h>
#include <vector>
#include <string>
using namespace std;
//#include "vista.h"

class path
{
	public:
		 path(void);
		~path(void);



		void PathInfo(int, int, int, int, int, int, CString );
		int PathID;
		int Org;
		int Dest;
		int Size ;
		int Freeflowtt;
		int Length;
		CString links_string;
		int returnPathID();
		int returnOrg();
		int returnDest();
		int returnSize();
		int returnFreeflowtt();
		int returnLength();
		CString returnLinks();
		std::vector <int> returnLinklist();
		void PathLinks(CString &);
		void trackings(int id, int type, int dta_departure, int sim_departure, int sim_exittime, int dta_path, int sim_path, CString arrivaltime);

		void replaceLinks(vector <vector<int>> replace);

		std::vector <int> VehIDs;
		std::vector < vector<int> > vehtimes;

		int DTA_Departure;
		int Sim_Exit;
		int Veh_Path_ID;
		CString ArrivalTime;
		std::vector <int> returnArrivalTime();
		void VehTimes(CString &);
		vector<int> returnVehIDs();		
		vector<vector<int>> returnVehTimes();


		std::vector <char> linklist;
		std::vector <int> links;
		std::vector <int>::iterator it;	

		vector <vector <int>> replace_link_id;

		//std::vector <char> vehlist;

};
#endif