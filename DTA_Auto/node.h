#ifndef NODE_H
#define NODE_H

#pragma once

#include <iostream>
#include <string>
using namespace std;
#pragma once
#include <vector>
#include <algorithm>    // std::transform
class node{

public: 
	node(void);
	~node(void);

	int vsta_ID();
	
	void newType(int type);
	int returnVstaType();


	int vsta_node_id;
	int vsta_type;

	//vista
	void  vista_locate(int, int, float, float);
	float vsta_X();
	float vsta_Y();
	float vsta_node_x;
	float vsta_node_y;

	//corsim
	void node::corsim_position(int id, float x, float y);
	int crsm_node_id;	
	int crsm_node_x;
	int crsm_node_y;
	int crsm_X();
	int crsm_Y();
	int crsm_ID();
	void inputCrsmID(int id );

	void corsim_node_locate(int, int, int, int);

	void  UpNodes(vector <int> up);
	vector <int> returnUpNodes();
	std::vector <int> upnodes;
	
	
	void input(int id, int phase, int,  int timered, int timeyellow, int timegreen, CString cs,CString vs);
	int returnSignalID();
	vector <int> returnPhaseOrder();
	vector <vector <int>> returnPhaseTime();

	void InputLinkFrom(CString & links_string);
	void InputLinkTo(CString &);

	vector <vector <int>> returnLinkFrom();
	vector <vector <int>> returnLinkTo();

	void  InputNodeFrom(vector<int> nodes);
	void InputNodeTo(vector<int> nodes);
	vector <vector <int>> returnNodeFrom();	
	vector <vector <int>> returnNodeTo();
	std::vector <int>::iterator it;	


	void RecordPhaseColor(vector<vector<int>>,  vector<vector<int>> );

	void RecordPhaseColor( vector<vector<int>> phasecolor);
	vector<vector<int>> returnUniqueUplinks();
	vector<vector<int>> returnPhaseColor();

	std::vector <vector <int>> UniqueUplinks;
	std::vector <vector <int>> PhaseColor;

	std::vector <int> phase_order;  //the input phase
	std::vector <vector <int>> phase_time; // 0 = green, 1 = yellow, 2 = all-red
	std::vector <vector <int>> phase_linkfrom; // 
	std::vector <vector <int>> phase_linkto;

	int Corsim_node;
	void inputCorsimNode(int node);
	int returnCorsimNode();

	std::vector <vector <int>> node_from;
	std::vector <vector <int>> node_to;

	int Signal_ID;
	int Phases;
	int Red;
	int Green;
	int Yellow;
	CString LinkFrom;
	CString LinkTo;

	void offset(int offsettime);
	int returnoffset();
	int OffSet;




};
#endif