﻿#ifndef SSH_H
#define SSH_H

#include <libssh/libssh.h>
#include <libssh/sftp.h>

#include <sys/types.h>
#include <sys/stat.h>

#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h> 
#include <cstdlib>
#include <iostream>
#include <string.h>
#include <errno.h>
#include <tchar.h>
#include <Windows.h>
#include <string>
#include<fstream>
#include <deque>

using namespace std;
class SSH
{
	public:
		SSH(void);
		~SSH(void);

		ssh_session connectSSH(char*);
		int show_remote_files(ssh_session);
		int getSubnetwork(ssh_session, sftp_session, ssh_channel);
		int get_link_node_data(ssh_session , sftp_session );
		int read_slice(ssh_session, sftp_session, char *);

		int revise_slice(ssh_session, sftp_session, char *);
		int extract_demand(ssh_session, sftp_session, ssh_channel);



		
};
#endif
