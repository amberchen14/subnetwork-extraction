﻿
#include "stdafx.h"
#include "SSH.h"

SSH :: SSH(){}
SSH ::~SSH(){}

char *password= "#Yj17131"; 
char account_name[] = "ac59434";
char* network_name;

ssh_session SSH :: connectSSH(char* name)
{
	ssh_session my_ssh_session;
	int port = 22;
	network_name = name;

	my_ssh_session = ssh_new();
	if (my_ssh_session == NULL)
    exit(-1);
	
	//connect to the server
	ssh_options_set(my_ssh_session, SSH_OPTIONS_HOST, "nmc-compute1.ctr.utexas.edu");
	ssh_options_set(my_ssh_session, SSH_OPTIONS_USER, account_name);
	ssh_options_set(my_ssh_session, SSH_OPTIONS_PORT, &port);
	int rcnt = ssh_connect(my_ssh_session);
	if (rcnt != SSH_OK)
	{
		fprintf(stderr, "Error connecting to localhost: %s\n",ssh_get_error(my_ssh_session));
		exit(-1);
	}

	rcnt= ssh_userauth_password(my_ssh_session, NULL, password);
	
	if (rcnt != SSH_AUTH_SUCCESS)
	{
		fprintf(stderr, "Error authenticating with password: %s\n",ssh_get_error(my_ssh_session));
		ssh_disconnect(my_ssh_session);
		ssh_free(my_ssh_session);  
		exit(-1);
	}
	return my_ssh_session;
}

int SSH:: show_remote_files(ssh_session session)
{
  ssh_channel channel;

  int rc;
  channel = ssh_channel_new(session);
  if (channel == NULL)	return SSH_ERROR;

  rc = ssh_channel_open_session(channel);
  if (rc != SSH_OK)
  {
    ssh_channel_free(channel);
    return rc;
  }
  
  rc = ssh_channel_request_pty(channel);
  if (rc != SSH_OK) return rc;
  
  rc = ssh_channel_request_shell(channel);
  if (rc != SSH_OK) return rc;
  

  sftp_session sftp;
  sftp = sftp_new(session);
  if (sftp == NULL)
  {
    fprintf(stderr, "Error allocating SFTP session: %s\n",
            ssh_get_error(session));
    return SSH_ERROR;
  }
  rc = sftp_init(sftp);
  if (rc != SSH_OK)
  {
    fprintf(stderr, "Error initializing SFTP session: %s.\n",
            sftp_get_error(sftp));
    sftp_free(sftp);
    return rc;
  }
  
  getSubnetwork(session,sftp,channel);
  extract_demand(session,sftp, channel);
  return SSH_OK;
}

int SSH:: getSubnetwork(ssh_session session, sftp_session sftp, ssh_channel channel)
{
	int rc;
	char buffer[16];
	unsigned int nbytes;
	char inbuffer[20][256];

	if (ssh_channel_is_open(channel) && !ssh_channel_is_eof(channel))
	{
		for (int i = 0; i < 16; i ++)
		{
			if (i == 0)		nbytes = sprintf(inbuffer[i], "vista-data fix subnetstudy downtown_pm_base_project_comparison_Amber \n" );		
			if (i == 1)		nbytes = sprintf(inbuffer[i], "/opt/vtg/bin/vista-copy subnetstudy downtown_pm_base_project_comparison_Amber subnet2012 subnetstudy %s -d \" no description\" -o \n", network_name);		
			if (i == 2)		nbytes = sprintf(inbuffer[i], "cd /var/opt/vtg/vista/subnetstudy/%s\n", network_name);		
			if (i == 3)
			{
				// put the link.txt and node.txt into subnetwork database
				 get_link_node_data(session, sftp);			 
					 continue;
			}
			if (i == 4)		nbytes = sprintf(inbuffer[i], "psql -U subnetstudy -d subnetstudy_%s\n", network_name);		
			if (i == 5)		nbytes = sprintf(inbuffer[i], "drop table subnet_nodes;drop table subnet_links;\n");
			if (i == 6)		nbytes = sprintf(inbuffer[i], "create table subnet_nodes (NODE_ID integer);\n");
			if (i == 7)		nbytes = sprintf(inbuffer[i], "create table subnet_links (LINK_ID integer);\n");
			if (i == 8)		nbytes = sprintf(inbuffer[i], "\\copy subnet_nodes from subnet_nodes.txt \n");
			if (i == 9)		nbytes = sprintf(inbuffer[i], "\\copy subnet_links from subnet_links.txt \n");
			if (i == 10)	nbytes = sprintf(inbuffer[i], "Delete from links where id not in (select link_id from subnet_links);\n Delete from linkdetails where id not in (select link_id from subnet_links);\n Delete from nodes where id not in (select node_id from subnet_nodes);\n");		
			if (i == 11)	nbytes = sprintf(inbuffer[i], "Delete from signals where id not in (select node_id from subnet_nodes);\n Delete from phases where nodeid not in (select node_id from subnet_nodes);\n Delete from prhb_links where link not in (select link_id from subnet_links);\n ");		
			if (i == 12)	nbytes = sprintf(inbuffer[i], "Delete from linkbays where id not in (select link_id from subnet_links);\n");
			if (i == 13)	nbytes = sprintf(inbuffer[i], "Delete from demand;Delete from static_od;Delete from dynamic_od;Delete from demand_data;\n");
			if (i == 14)	nbytes = sprintf(inbuffer[i], "\\q \n");
			if (i == 15)	nbytes = sprintf(inbuffer[i], "cd /var/opt/vtg/vista/subnetstudy/subnet_script/subnet_script_test/\n");

			rc= ssh_channel_write(channel, inbuffer[i],nbytes);
			if (rc == SSH_ERROR)return SSH_ERROR;
			
			if (i == 1)	Sleep(180000);
			nbytes = ssh_channel_read_nonblocking(channel, buffer, sizeof(buffer), 0);
			while (nbytes > 0)
			{
				Sleep(100);
				if (fwrite(buffer, 1, nbytes, stdout) != nbytes)
				{
					ssh_channel_close(channel);
					ssh_channel_free(channel);
					return SSH_ERROR;
				}
				nbytes = ssh_channel_read_nonblocking(channel, buffer, sizeof(buffer), 0);
			}
		}
		if (nbytes <0)
		{
			ssh_channel_close(channel);
			ssh_channel_free(channel);
			return SSH_ERROR;
		}
	}
	return rc;
}

int SSH:: get_link_node_data(ssh_session session, sftp_session sftp)
{
  int access_type = O_WRONLY | O_CREAT | O_TRUNC;
  sftp_file file;
  int rc;
  int nwritten;
  string subnet[2];
  subnet[0] = "C:\\Users\\user\\Desktop\\auto_test\\subnet_links.txt";
  subnet[1] = "C:\\Users\\user\\Desktop\\auto_test\\subnet_nodes.txt";
  for (int i = 0; i < 2; i ++)
  {
	  char buffer[2][128];
	  sprintf(buffer[0], "/var/opt/vtg/vista/subnetstudy/%s/subnet_links.txt", network_name);
	  sprintf(buffer[1], "/var/opt/vtg/vista/subnetstudy/%s/subnet_nodes.txt", network_name);	
	  file = sftp_open(sftp, buffer[i],access_type,S_IEXEC); //S_IEXEC
	
	// if (i == 0) file = sftp_open(sftp, "/var/opt/vtg/vista/subnetstudy/test4/subnet_links.txt",access_type,S_IEXEC); //S_IEXEC
	// if (i == 1) file = sftp_open(sftp, "/var/opt/vtg/vista/subnetstudy/test4/subnet_nodes.txt",access_type,S_IEXEC); //S_IEXEC
	 if (file == NULL)
	 {
		 fprintf(stderr, "Can't open file for writing: %s\n",ssh_get_error(session));
		 return SSH_ERROR;
	 }
	 ifstream fin(subnet[i], ios::binary);
	 if (fin) 
	 {
		 fin.seekg(0, ios::end);
		 ios::pos_type bufsize = fin.tellg(); // get file size in bytes
		 fin.gcount();
		 fin.seekg(0); // rewind to beginning of file
		 char* buf = new char[bufsize];
		  fin.read(buf, bufsize); // read file contents into buffe
		  nwritten= sftp_write(file, buf, bufsize); // write to remote file
	  } 
	   if (nwritten != fin.gcount()) //fin.gcount()
	  {
		fprintf(stderr, "Can't write data to file: %s\n",ssh_get_error(session));
		sftp_close(file);
		return SSH_ERROR;
	  }
	  rc = sftp_close(file);
	  Sleep(1000);
	  if (rc != SSH_OK)
	  {
		fprintf(stderr, "Can't close the written file: %s\n",ssh_get_error(session));
		return rc;
	  }
  }
  return SSH_OK;
}

int SSH::extract_demand(ssh_session session, sftp_session sftp, ssh_channel channel)

{
	int rc;
	char buffer[128];
	unsigned int nbytes;
	char inbuffer[65][128];

	if (ssh_channel_is_open(channel) && !ssh_channel_is_eof(channel))
	{
		for (int i = 0; i < 53; i ++)
		{
			if (i == 0)	nbytes = sprintf(inbuffer[i], "cd /var/opt/vtg/vista/subnetstudy/subnet_script_test \n");
			if (i == 1)	nbytes = sprintf(inbuffer[i], "psql -U subnetstudy -d subnetstudy_%s\n", network_name);									//need change subnetwork name
			if (i == 2)	nbytes = sprintf(inbuffer[i], "\\copy linkdetails to ld_subnet_%s.txt\n", network_name);									//need change subnetwork name
			if (i == 3)	nbytes = sprintf(inbuffer[i], "\\q \n");
			if (i == 4)
			{
				read_slice(session, sftp, network_name);
				continue;
			}
			if (i == 5) 
			{
				nbytes = sprintf(inbuffer[i],"javac -Xlint Subnet_Slice_v3.java\n");
			}
			if (i == 6) nbytes = sprintf(inbuffer[i],"java Subnet_Slice_v3 \n");
			if (i == 7)	nbytes = sprintf(inbuffer[i], "psql -U subnetstudy -d subnetstudy_%s\n",network_name);									//need change subnetwork name
			if (i == 8)	nbytes = sprintf(inbuffer[i], "create table demand_data (origin integer,dest integer,ast integer,veh integer,olink integer,dlink integer);\n");
			if (i == 9)	nbytes = sprintf(inbuffer[i], "\\copy demand_data from demand_sub_%s.txt\n",network_name);								//need change subnetwork name
			
			if (i == 10)nbytes = sprintf(inbuffer[i], "CREATE OR REPLACE FUNCTION get_link_points( linkid integer ) RETURNS PATH  AS $$ DECLARE  result PATH;\n");
			if (i == 11)nbytes = sprintf(inbuffer[i], "	BEGIN SELECT INTO result ( '[('||a.x||','||a.y||'),('||b.x||','||b.y||')]') from nodes a,nodes b,linkdetails c where a.id=c.source and b.id=c.destination and c.id=$1 ; \n");
			if (i == 12)nbytes = sprintf(inbuffer[i], " RETURN result;	END; $$ LANGUAGE 'plpgsql';\n");
			
			// insert demand for the subnetwork database
			if (i == 13) nbytes = sprintf(inbuffer[i], " insert into dynamic_od select 1,veh,origin,dest, count(*),ast from demand_data group by origin,dest,veh,ast;\n");
			if (i == 14) nbytes = sprintf(inbuffer[i], " insert into nodes select distinct a.origin, 300, b.x, b.y from dynamic_od a, nodes b where (b.id =a.origin-150000) and (a.origin not in (select id from nodes));\n");
			if (i == 15) nbytes = sprintf(inbuffer[i], " insert into nodes select distinct a.destination, 400, b.x, b.y from dynamic_od a, nodes b where (b.id=a.destination-250000) and (a.destination not in (select id from nodes));\n");
			if (i == 16) nbytes = sprintf(inbuffer[i], " insert into linkdetails select id+100000, 300, id, id-150000, 300, 1, 100000, 1 from nodes where type =300;\n");
			if (i == 17) nbytes = sprintf(inbuffer[i], " insert into linkdetails select id+100000, 400, id-250000, id, 300, 1, 100000, 1 from nodes where type =400;\n");
			if (i == 18) nbytes = sprintf(inbuffer[i], " insert into links (id, points) select id, get_link_points(b.id) from linkdetails b where b.id not in (select id from links);\n");
			
			if (i == 19) nbytes = sprintf(inbuffer[i], " update linkdetails set type=100 where type>100;\n");
			if (i == 20) nbytes = sprintf(inbuffer[i], " update nodes set type=100 where type>100;\n");
			if (i == 21) 
			{
				Sleep(5000);
				nbytes = sprintf(inbuffer[i]," delete from signals where id+150000 in (select id from nodes) or id+250000 in (select id from nodes);\n");
			}
			if (i == 22)	nbytes = sprintf(inbuffer[i], "delete from phases where nodeid not in (select id from signals);\n");
			if (i == 23)	nbytes = sprintf(inbuffer[i], "delete from bus;\n");
			if (i == 24)	nbytes = sprintf(inbuffer[i], "delete from bus_route_link;\n");
			if (i == 25)	nbytes = sprintf(inbuffer[i], "delete from bus_frequency;\n");
			if (i == 26)	nbytes = sprintf(inbuffer[i], "delete from bus_period;\n");
			if (i == 27)	nbytes = sprintf(inbuffer[i], "delete from bus_stop;\n");			
			if (i == 28)	nbytes = sprintf(inbuffer[i], "\\q \n");
			
			//Prepare network 
			if (i == 29)	nbytes = sprintf(inbuffer[i], "cd /opt/vtg/libexec/vista\n");
			if (i == 30)	nbytes = sprintf(inbuffer[i], "/opt/vtg/libexec/vista/manage-data -n %s -P subnet2012 -U subnetstudy -a -r -p -k\n", network_name);    //need change network name 
			if (i == 31)	nbytes = sprintf(inbuffer[i], "psql -U subnetstudy -d subnetstudy_%s\n",network_name);												//need change network name 
			
			//Correct for link length and cell length issues
			if (i == 32)	nbytes = sprintf(inbuffer[i], "update celldata set jamd=(select cast (6 as numeric)/60*speed*5280*lanes/19*1000 from linkdetails b \n");
			if (i == 33)	nbytes = sprintf(inbuffer[i], "where celldata.link=b.id) where link in (select d.id from linkdetails d,celldata e where d.id=e.link and (cast\n");
			if (i == 34)	nbytes = sprintf(inbuffer[i], "(6 as numeric)/60*speed*5280)*lanes/19 >(cast(jamd as numeric)/1000+20));\n");

			if (i == 35)	nbytes = sprintf(inbuffer[i], "update celldata set jamd=(select cast (6 as numeric)/60*speed*5280*lanes/19*1000 from linkdetails b\n");
			if (i == 36)	nbytes = sprintf(inbuffer[i], "where celldata.link=b.id) where link in (select d.id from linkdetails d,celldata e where d.id=e.link and (cast\n");
			if (i == 37)	nbytes = sprintf(inbuffer[i], "(6 as numeric)/60*speed*5280)*lanes/19 >(cast(jamd as numeric)/1000+20) and d.capacity>2000 and d.length<=500);\n");
			
			if (i == 38)	nbytes = sprintf(inbuffer[i], "select * from linkdetails d,celldata e where d.id=e.link and (cast (6 as numeric)/60*speed*5280)*lanes/19 >(cast(jamd as numeric)/1000+20);\n");
			if (i == 39)	nbytes = sprintf(inbuffer[i], "select * from linkdetails d,celldata e where d.id=e.link and (cast (6 as numeric)/60*speed*5280)*lanes/19 >(cast(jamd as numeric)/1000+20)\n"); 
			if (i == 40)	nbytes = sprintf(inbuffer[i], "and capacity>2000 and length<=500;\n");
			
			//Prepare demand (cannot do it automatically)
			if (i == 41)	
			{
				string response = "N";
				while (response != "Y")
				{
					cout << "Please Prepare Demand, after that, please press y\n";
					getline(cin, response);
					if (response == "y" || response == "Y") break;
				}
				continue;
			}
			if (i == 42) nbytes = sprintf(inbuffer[i], "\\q \n"); 
			
			//Validate data
			if (i == 43) nbytes = sprintf(inbuffer[i], "/opt/vtg/libexec/vista/sanity-check -n %s -P subnet2012 -U subnetstudy\n", network_name);    //need change network name			
			
			//correct common errors
			if (i == 44)	nbytes = sprintf(inbuffer[i], "psql -U subnetstudy -d subnetstudy_%s\n", network_name);												//need change network name 			
			if (i == 45)	nbytes = sprintf(inbuffer[i], "delete from signals where id+150000 in (select id from nodes) or id+250000 in (select id from nodes);\n");  
			if (i == 46)	nbytes = sprintf(inbuffer[i], "delete from phases where nodeid not in (select id from signals);\n");
			if (i == 47)	nbytes = sprintf(inbuffer[i], "delete from bus;\n");

			if (i == 48)	nbytes = sprintf(inbuffer[i], "delete from bus_route_link;\n");  
			if (i == 49)	nbytes = sprintf(inbuffer[i], "delete from bus_frequency;\n");
			if (i == 50)	nbytes = sprintf(inbuffer[i], "delete from bus_period;\n");
			if (i == 51)	nbytes = sprintf(inbuffer[i], "delete from bus_stop;\n");
			if (i == 52)	nbytes = sprintf(inbuffer[i], "\\q \n");

			rc= ssh_channel_write(channel, inbuffer[i],nbytes);
			if (rc == SSH_ERROR)return SSH_ERROR;
			Sleep(500);
			nbytes = ssh_channel_read_nonblocking(channel, buffer, sizeof(buffer), 0);
			while (nbytes > 0)
			{
				Sleep(100);
				if (fwrite(buffer, 1, nbytes, stdout) != nbytes)
				{
					ssh_channel_close(channel);
					ssh_channel_free(channel);
					return SSH_ERROR;
				}
				nbytes = ssh_channel_read_nonblocking(channel, buffer, sizeof(buffer), 0);
			}
		}
		if (nbytes <0)
		{
			ssh_channel_close(channel);
			ssh_channel_free(channel);
			return SSH_ERROR;
		}
	}
	return rc;
}

#define MAX_XFER_BUF_SIZE 64
char slice_v3[12000];
int SSH:: read_slice(ssh_session session, sftp_session sftp, char * name)
{
	int access_type;
	sftp_file file;
	char buffer[MAX_XFER_BUF_SIZE];
//	char buffer1[60];
	int async_request;
	int nbytes;
	long counter;
	int rc;
	string script;
	access_type = O_RDONLY;

	file = sftp_open(sftp, "/var/opt/vtg/vista/subnetstudy/subnet_script_test/Subnet_Slice_v3_backup.java",
		access_type, S_IEXEC);
	
	if (file == NULL) 
	{
		fprintf(stderr, "Can't open file for reading: %s\n",ssh_get_error(session));
		return SSH_ERROR;
	}
	sftp_file_set_nonblocking(file);
	async_request = sftp_async_read_begin(file, sizeof(buffer));
	counter = 0L;
	Sleep(2000);
	if (async_request >= 0) nbytes = sftp_async_read(file, buffer, sizeof(buffer),async_request); 
	else nbytes = -1;
	int a = SSH_AGAIN;
	int count = 0;
	int jump = 0;
	while (nbytes > 0 || nbytes == SSH_AGAIN)
	{
		for (int i = 0; i < MAX_XFER_BUF_SIZE; i ++) 
		{
			
			slice_v3[i	+ count * MAX_XFER_BUF_SIZE] = buffer[i];	
		}
		if (nbytes > 0) 
		{
			string strChar = string(buffer);
			cout<<strChar;
			count++;
			async_request = sftp_async_read_begin(file, sizeof(buffer));
		}
		else counter++;
	
		Sleep(10);
		if (async_request >= 0) nbytes = sftp_async_read(file,buffer, sizeof(buffer),async_request);
		else nbytes = -1;
	}
	if (nbytes < 0)
	{
		fprintf(stderr, "Error while reading file: %s\n",ssh_get_error(session));
		sftp_close(file);
		return SSH_ERROR;
	}
	network_name = name;
	revise_slice(session, sftp, network_name);
	
	printf("The counter has reached value: %ld\n", counter);
	rc = sftp_close(file);
	if (rc != SSH_OK) 
	{
		fprintf(stderr, "Can't close the read file: %s\n",ssh_get_error(session));
		return rc;
	}
	return SSH_OK;
}

int SSH:: revise_slice(ssh_session session, sftp_session sftp, char* name ) //string script
{
	int access_type = O_WRONLY | O_CREAT | O_TRUNC;
	sftp_file file;
	int rc;
	int nwritten;

	network_name = name;
	char * pch;
	char link_data[]= "static String link_data =";
	char link_replace[96];

	sprintf(link_replace,"static String link_data = \"ld_subnet_%s.txt\";", network_name);
	
	char output_file[] = "static String output_file =";
	char output_replace[96];
	sprintf(output_replace," static String output_file = \"demand_sub_%s.txt\";", network_name);
	
	deque <char> replace_name[2];
	for (int i = 0; i < sizeof(link_replace); i ++)
	{
		replace_name[0].push_back(link_replace[i]);
		if (link_replace[i] == ';') break;
	}

	for (int i = 0; i < sizeof(output_replace); i ++)
	{
		replace_name[1].push_back(output_replace[i]);
		if (output_replace[i] == ';') break;
	}
	
	for (int i = 0; i < 2; i ++)
	{
		char *pBuf = NULL;
		int BufSize = replace_name[i].size();
		pBuf = new char [BufSize];
		int posi = 0;
		for (std::deque<char>::iterator pos = replace_name[i].begin(); pos!= replace_name[i].end(); pos++)
		{
			pBuf[posi]=(*pos);
			posi++;
		}		
		if (i == 1)	pch = strstr (slice_v3,output_file);	
		if (i == 0) pch = strstr (slice_v3,link_data);
		strncpy (pch, pBuf,BufSize);
		puts(slice_v3);
	}
	deque <char> revise_slice;					//get new script size
	for (int i = 0; i < sizeof(slice_v3); i ++)
	{
		if (slice_v3[i] == '$') 
			break;
		revise_slice.push_back(slice_v3[i]);
	}

	char *pRevise = NULL;						//put the content in
	int RevSize = revise_slice.size();
	pRevise = new char [RevSize];
	int rpos = 0;
	for (std::deque<char>::iterator pos = revise_slice.begin(); pos!= revise_slice.end(); pos++)
	{
		pRevise[rpos]=(*pos);
		rpos++;
	}	

	file = sftp_open(sftp, "/var/opt/vtg/vista/subnetstudy/subnet_script_test/Subnet_Slice_v3.java",access_type,S_IEXEC); //S_IEXEC
	if (file == NULL)
	{
		fprintf(stderr, "Can't open file for writing: %s\n",ssh_get_error(session));
		return SSH_ERROR;
	}	

	nwritten= sftp_write(file, pRevise, RevSize); // write to remote file script/*.c_str()*/
	if (nwritten != RevSize) //fin.gcount()
	{
		fprintf(stderr, "Can't write data to file: %s\n",ssh_get_error(session));
		sftp_close(file);
		return SSH_ERROR;
	}
	rc = sftp_close(file);
	if (rc != SSH_OK)
	{
		fprintf(stderr, "Can't close the written file: %s\n",ssh_get_error(session));
		return rc;
	}
	return SSH_OK;
}
