#ifndef LINK_H
#define LINK_H

#pragma once

#include <iostream>
#include <string>
#include <fstream>
using namespace std;
#pragma once
#include <deque>
#include <vector>


class link{

public: 
	 link(void);
	~ link(void);

	int vista_id;
	int vista_type;	
	int vista_org;
	int vista_dest;
	int corsim_org;
	int corsim_dest;
	float vista_length;
	float vista_speed;
	int corsim_speed;
	int vista_capacity;
	int vista_lanes;
	vector <int> Orient;
	int angle;
	vector <int> dn_corsim_nodes;
	vector <int> dn_vista_links;
	int OppositeLink;
	int NumDnLinks; 
	vector <vector <int>> downlinks;
	float corsim_length;
	int replace_vista_id;


	vector <vector <int>> dnsvehtimes; //downstream's vehicle and time
	
	void details(int id, int type, int org, int dest, float length, float speed, int capacity, int lanes);
	void inputVistaID(int id);
	void inputCorsimUp(int up);
	void inputCorsimDn(int dn);


	void newType(int type);	
	int returnVistaID();
	int returnVistaType();
	int returnVistaOrg();
	int returnVistaDest();
	void CosimNodes(int up, int dn);
	int returnCorsimUp();
	int returnCorsimDn();
	float returnVistaLength();
	float returnVistaSpeed();
	int returnCorsimSpeed();
	int returnVistaCapacity();
	int returnVistaLanes();
	vector <int> returnDownstreams();
	vector <int> returnDownNodes();
	void orientation(vector <int> orient);
	vector <int> returnOrient();
	float returnCORSIMLength();
	int  returnAngle();
	void Downstreams(int, vector<vector <int>>);
	int returnNumDns();
	void Twoways(int id);
	int returnTwoways();
	void combineLinkFlow(vector<vector <int>> flow);
	void DnsVehs(vector <int> );
	vector <vector <int> >returnDnsVehs(int dns) ;
	vector<vector <int>> returnLinkFlow();
	void replaceLink(int linkid);
	int returnReplaceLink();
	void inputCorsimLength(float length);


	vector<vector <int>> returnVehs();
	/*

	int id;
	int org;
	int link_dest;
	int link_flag;


	void downstream(int, int);
	int downlk();
	int downnode();

	vector <char> rTRF;

	void getLinks(int, int, int, int, int, int, int, int, int);
	void subLinks(int , int );

	int Flag1;
	int Flag2;
	int returnNode1();
	int returnNode2();
	int returnLeft();
	int returnRight();
	int returnThru();
	int returnDiag();
	int returnOpos();
	int returnFlag1();
	int returnFlag2();

	int Node1;
	int Node2;
	int Left;
	int Thru;
	int Right;
	int Diag;
	int RightDiag;
	int Opos;
	int apt[5];

	void SubNetwork(int , int , int , int , int , int );

	void SignalControl(int, int, int, int, int, int);
	int returnAPT1();
	int returnAPT2();
	int returnAPT3();
	int returnAPT4();
	int returnAPT5();
	*/

};
#endif