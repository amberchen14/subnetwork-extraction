#ifndef FAKE_NODE_H
#define FAKE_NODE_H

#pragma once

#include <iostream>
#include <string>
using namespace std;
#pragma once
#include <vector>
#include <algorithm>    // std::transform
class fake_node{

public: 
	fake_node(void);
	~fake_node(void);

	
	void details(int vista_id, int corsim_id, int x, int y);	
	int vsta_copy_id;
	int crsm_fake_id;
	int crsm_node_x;
	int crsm_node_y;
	int ReturnCrsm_X();
	int ReturnCrsm_Y();
	int ReturnCrsm_ID();
	int ReturnVsta_ID();

};
#endif