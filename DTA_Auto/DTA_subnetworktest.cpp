﻿// DTA_subnetwork.cpp : Defines the entry point for the console application.

#include "stdafx.h"
#include <windows.h>
#include <odbcinst.h>
#include "linklist.h"
//#include "node.h"
#include "CSpreadSheet.h"
using namespace std;
#include <stdlib.h>
#include <crtdbg.h>
#include <string>
#include <deque>

deque <linklist> lklist;
deque <linklist> sublk;
deque <linklist> temlk;

deque <linklist> :: iterator lk_pos;
deque <linklist> :: iterator sublk_pos;
//deque <node> ndlist;
//deque <node> ::iterator nd_pos;

int _tmain(int argc, _TCHAR* argv[])
{
	//_CrtSetBreakAlloc(18166);
	//_CrtSetBreakAlloc(18153);
	//_CrtSetBreakAlloc(18164);

	CSpreadSheet SS("W:\\Amber_DTA\\regional_network.xls", "linkdetails");
	//CStringArray Rows, Column;
	CString CValue;

	int iCols = SS.GetTotalColumns();   // 總列數
	int iRows = SS.GetTotalRows();    // 總行數
	int iCurRow = SS.GetCurrentRow(); // 當前所在行號

	fprintf(stderr, "column: %d, row: %d", iCols, iRows);
	
	int cols_pos;
	int rows_pos;

	int id;
	int org;
	int dest;
	int flag = 0; //=0 unread, =1 read
	for (rows_pos = 2;  rows_pos <= iRows; rows_pos++)
	{
		cols_pos = 1;
		SS.ReadCell(CValue,cols_pos, rows_pos);
		id=atoi(CValue);

		cols_pos ++;
		SS.ReadCell(CValue, cols_pos, rows_pos);
		org=atoi(CValue);		
		cols_pos ++;
		SS.ReadCell(CValue, cols_pos, rows_pos);
		dest=atoi(CValue);		
		
		linklist lk;
		lk.details(id, org, dest, flag); 
		lklist.push_back(lk);
	
		lklist.size();
		//fprintf(stderr, "%d, %d, %d", id, org, dest);			
		//CString str;
		//str.Format("%d, %d, %d", id, org, dest);
		//AfxMessageBox(str);
		
	}
	FILE *fexcel;
	char fileexel[] = ("W:\\Amber_DTA\\test.xls");
	fexcel = fopen( fileexel, "w");
	int size = lklist.size();
	fprintf(fexcel,"size \t %d", size);
	fprintf(fexcel,"\n");
/*
	for (lk_pos = lklist.begin();  lk_pos !=lklist.end(); lk_pos++)
	{
		id = (*lk_pos).ID();
		org=(*lk_pos).ORG();
		dest=(*lk_pos).DEST();

		fprintf(fexcel,"id\t %d \t org\t %d \t dest\t %d", id, org, dest);
		fprintf(fexcel,"\n");
	}
	*/

	
	int startlink = 118172;	
	int order = 5;

	for (int runtimes = 0; runtimes < 2; runtimes++)
	{	
		for (lk_pos = lklist.begin();  lk_pos !=lklist.end(); lk_pos++)
		{
			if ((*lk_pos).FLAG()==1) continue;
			if (startlink ==(*lk_pos).ID() || (org==(*lk_pos).DEST() && dest==(*lk_pos).ORG() ))
			{
				id = (*lk_pos).ID();
				org = (*lk_pos).ORG();
				dest = (*lk_pos).DEST();
				flag = 1;
				(*lk_pos).details(id, org, dest, flag);
					
				flag = 0; //parameter order
				linklist lk;
				lk.details(id, org, dest,flag);
				sublk.push_back(lk);

				fprintf(fexcel,"id\t %d \t org\t %d \t dest\t %d\t flag \t %d \t",id, org, dest, flag);						
				fprintf(fexcel,"\n");
			}
		}
	}

	for (int i = 1; i <= order + 1  ; i ++) 
	{
		for (sublk_pos= sublk.begin(); sublk_pos != sublk.end(); sublk_pos++ )
		{
			int org1 = (*sublk_pos).DEST();
			int org2 = (*sublk_pos).ORG();
			flag = (*sublk_pos).FLAG();
			//if (flag == i) continue;
			if ((*sublk_pos).ID() == 105201)
			{
				int	abc=456;
			}
			if (i < order +1 )
			{
				for (lk_pos = lklist.begin();  lk_pos !=lklist.end(); lk_pos++)
				{
					if ((*lk_pos).FLAG()== 1) continue;
					if ((*lk_pos).ID() < 400000 && (*lk_pos).ID() > 200000) continue;
					if ((*lk_pos).ORG()== org1 || (*lk_pos).ORG() == org2 || (*lk_pos).DEST() == org1 || (*lk_pos).DEST()==org2)
					{
						if ((*lk_pos).ID() == 18191)
						{
						int	abc=456;
						}
						(*lk_pos).details((*lk_pos).ID(), (*lk_pos).ORG(), (*lk_pos).DEST(), 1); //flag = 1 read
							
						int sub_flag = i; //parameter order
						linklist lk;
						lk.details((*lk_pos).ID(), (*lk_pos).ORG(), (*lk_pos).DEST(), sub_flag);
						temlk.push_back(lk);

						//fprintf(fexcel,"id\t %d \t org\t %d \t dest\t %d\t flag \t %d \t expect order \t %d \t", sub_id, sub_org, sub_dest, sub_flag, order);
						fprintf(fexcel,"id\t %d \t org\t %d \t dest\t %d\t flag \t %d \t expect order \t %d",(*lk_pos).ID(),(*lk_pos).ORG(), (*lk_pos).DEST(), sub_flag, order);						
						fprintf(fexcel,"\n");	

						continue;							
					}
				
				}
			}
			else if (i == order + 1)
			{
				for (lk_pos = lklist.begin();  lk_pos !=lklist.end(); lk_pos++)
				{
					if ((*lk_pos).ID() > 400000 || (*lk_pos).ID() < 200000) continue;
					if ((*lk_pos).DEST()== org1 || (*lk_pos).ORG() == org2 || (*lk_pos).DEST() == org1 || (*lk_pos).DEST()==org2)
					{
						(*lk_pos).details((*lk_pos).ID(), (*lk_pos).ORG(), (*lk_pos).DEST(), 1); //flag = 1 read
							
						int sub_flag = i; //parameter order
						linklist lk;
						lk.details((*lk_pos).ID(), (*lk_pos).ORG(), (*lk_pos).DEST(), sub_flag);
						temlk.push_front(lk);

						//fprintf(fexcel,"id\t %d \t org\t %d \t dest\t %d\t flag \t %d \t expect order \t %d \t", sub_id, sub_org, sub_dest, sub_flag, order);
						fprintf(fexcel,"id\t %d \t org\t %d \t dest\t %d\t flag \t %d \t expect order \t %d",(*lk_pos).ID(),(*lk_pos).ORG(), (*lk_pos).DEST(), sub_flag, order);						
						fprintf(fexcel,"\n");										
						continue;							
					}
				
				}
			}
		}
		for( lk_pos = temlk.begin(); lk_pos != temlk.end(); lk_pos++)
		{
			int sub_id = (*lk_pos).ID();
			int sub_org = (*lk_pos).ORG();
			int sub_dest = (*lk_pos).DEST();	
			int sub_flag = order; //parameter order
		
			linklist lk;
			lk.details((*lk_pos).ID(), (*lk_pos).ORG(), (*lk_pos).DEST(), sub_flag);
			sublk.push_back(lk);
		}
	//	fprintf(fexcel,"sub subsize \t %d \n", sublk.size());
	//	fprintf(fexcel,"tem subsize \t %d \n", temlk.size());
		temlk.clear();
	}
/*
		for( lk_pos = lklist.begin(); lk_pos != lklist.end(); lk_pos++)
		{
			id = (*lk_pos).ID();
			org = (*lk_pos).ORG();
			dest = (*lk_pos).DEST();	
			flag = (*lk_pos).FLAG(); //parameter order
			fprintf(fexcel,"\n");
			fprintf(fexcel,"id\t %d \t org\t %d \t dest\t %d\t flag \t %d \t",id, org, dest, flag);						
		}
*/


	fclose(fexcel);
	/*

		FILE *fexcel1;
		char fileexel1[] = ("X:\\Amber_DTA\\test.xls");
		fexcel1 = fopen( fileexel1, "w");
		//int size = lklist.size();
		
		for (lk_pos = sublk.begin();  lk_pos !=sublk.end(); lk_pos++)
		{
			id = (*lk_pos).ID();
			org=(*lk_pos).ORG();
			dest=(*lk_pos).DEST();

			fprintf(fexcel1,"id\t %d \t org\t %d \t dest\t %d", id, org, dest);
			fprintf(fexcel1,"\n");			
		}
		fclose(fexcel1);
		break;
		int flag = 0;
		for(nd_pos = ndlist.begin(); nd_pos != ndlist.end();nd_pos++) //look for next downnode
		{
			int node = (*nd_pos).ReturnNode();
			for(lk_pos = lklist.begin();  lk_pos !=lklist.end(); lk_pos++) //get link upnode, downnode
			{
				id = (*lk_pos).ID();
				org = (*lk_pos).ORG();
				dest = (*lk_pos).DEST();

				if(node == org || node == dest)
				{
					for(sublk_pos = sublk.end();  sublk_pos !=sublk.begin(); sublk_pos--)
					{
						if(id ==((*sublk_pos).ID()))
							flag = 1; //link id exist
						break;						
					}
					if (flag == 0) //no link id
					{
						id = (*lk_pos).ID();
						org = (*lk_pos).ORG();
						dest = (*lk_pos).DEST();
						linklist lk;
						lk.details(id, org, dest);
						sublk.push_back(lk);


					}
				}
			}
		}*/

	system("PAUSE");
	return 0;	
}

