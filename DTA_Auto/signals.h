#ifndef SIGNALS_H
#define SIGNALS_H

#pragma once

#include <iostream>
#include <string>
using namespace std;
#pragma once
#include <vector>
#include <algorithm>    // std::transform

class signals{

public: 
	 signals(void);
	~ signals(void);

	
	void input(int id, int phase, int,  int timered, int timeyellow, int timegreen, CString cs,CString vs);
	int returnNodeID();
	vector <int> returnPhaseOrder();
	vector <vector <int>> returnPhaseTime();

	void InputLinkFrom(CString & links_string);
	void InputLinkTo(CString &);

	vector <vector <int>> returnLinkFrom();
	vector <vector <int>> returnLinkTo();

	void  InputNodeFrom(vector<int> nodes);
	void InputNodeTo(vector<int> nodes);
	vector <vector <int>> returnNodeFrom();	
	vector <vector <int>> returnNodeTo();
	std::vector <int>::iterator it;	


	void RecordPhaseColor(vector<vector<int>>,  vector<vector<int>> );
	vector<vector<int>> returnUniqueUplinks();
	vector<vector<int>> returnPhaseColor();

	std::vector <vector <int>> UniqueUplinks;
	std::vector <vector <int>> PhaseColor;

	std::vector <int> phase_order;  //the input phase
	std::vector <vector <int>> phase_time; // 0 = green, 1 = yellow, 2 = all-red
	std::vector <vector <int>> phase_linkfrom; // 
	std::vector <vector <int>> phase_linkto;

	int Corsim_node;
	void inputCorsimNode(int node);
	int returnCorsimNode();

	std::vector <vector <int>> node_from;
	std::vector <vector <int>> node_to;

	int Signal_ID;
	int Phases;
	int Node_ID;
	int Red;
	int Green;
	int Yellow;
	CString LinkFrom;
	CString LinkTo;
};
#endif