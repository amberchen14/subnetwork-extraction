﻿#include "stdafx.h"
#include "trfGeneration.h"


trfGeneration ::trfGeneration(){}
trfGeneration ::~trfGeneration(){}



int number_of_lines = 0;
int number_of_chars = 0;
int subnet_order;

vector <int> startlink;

void trfGeneration::IDTF(int node1, int node2)
{
	VISTA_NODE = node1;
	CM_NODE = node2;

	for (int j = 0; j < 80; j ++)	
	{
		TRF_210.push_back(' ');
		TRF_170.push_back(' ');
	}
	TRF_210.push_back('\n');
	TRF_170.push_back('\n');
	
	TRF_210[3]='1';
	TRF_210[7]='0';
	TRF_210[11]='0';
	TRF_210[77]='2';
	TRF_210[78]='1';
	TRF_210[79]='0';

	TRF_170[3]='0';
	TRF_170[77]='1';
	TRF_170[78]='7';
	TRF_170[79]='0';

}
void trfGeneration::getVistaLinks()
{	
	ifstream file ( "D:\\Amber\\corsim_test\\input\\linkdetails.csv" ); 
	//int iCols = CC.GetTotalColumns();   // 總列數
	//int iRows = CC.GetTotalRows();    // 總行數
	//int iCurRow = CC.GetCurrentRow(); // 當前所在行號
	//fprintf(stderr, "column: %d, row: %d", iCols, iRows);
	int id;
	int org;
	int dest;
	float length;
	float speed;
	int capacity;
	int lanes;
	int type;
	string value;
	int rows_pos = 1;
	while ( file.good())	
	{
		int id;
		int org;
		int dest;
		float length;
		float speed;
		int capacity;
		int lanes;
		int type;
		string value;

		getline ( file, value, ',' );
		if (value=="") 
			break;
		id=stoi(value);

		getline ( file, value, ',' );
		type=stoi(value);

		getline ( file, value, ',' );
		org=stoi(value);

		getline ( file, value, ',' );
		dest=stoi(value);

		getline ( file, value, ',' );
		length=stof(value);

		getline ( file, value, ',' );
		speed=stof(value);

		getline ( file, value, ',' );
		capacity=stoi(value);

		getline ( file, value, '\n' );
		lanes=stoi(value);

		link lk;
		lk.details(id, type, org, dest, length, speed, capacity, lanes); 
		linklist.push_back(lk);
		
		if (type == 100)
		{
			vector <int>inf;
			inf.push_back(id);
			inf.push_back(org);
			inf.push_back(dest);
			vista_connectors_nodes.push_back(inf);
		}
	}
}

double x_initial = 100000000;
double y_initial= 100000000;
void trfGeneration::getVistaNodes()
{	
	ifstream file ("D:\\Amber\\corsim_test\\input\\nodes.csv"); 

	int rows_pos = 1;
	int id;
	double x_axle;
	double y_axle;
	int flag; //=0 unread, =1 read
	int type;
	while ( file.good())	
	{
		string value ;	
		getline ( file, value, ',' );
		if (value=="") 
			break;
		id=stoi(value);
		
		getline ( file, value, ',' );
		type=stoi(value);

		getline ( file, value, ',' );
		x_axle=stod(value);

		getline ( file, value, '\n' );
		y_axle=stod(value);
		
		node nd;
		nd.vista_locate(id, type, x_axle, y_axle); 
		vsta_ndlist.push_back(nd);
		vsta_ndlist.size();	
		if (x_axle < x_initial)		x_initial = x_axle;	
		if ( y_axle < y_initial)	y_initial = y_axle;	
		/*
		for (lk_pos =linklist.begin(); lk_pos!= linklist.end(); lk_pos++)
		{
			int org = (*lk_pos).returnVistaOrg();
			int dest = (*lk_pos).returnVistaDest();
			if (org == id || dest== id)
			{
				node nd;
				nd.vista_locate(id, type, x_axle, y_axle); 
				vsta_ndlist.push_back(nd);
				vsta_ndlist.size();	
				if (x_axle < x_initial)		x_initial = x_axle;	
				if ( y_axle < y_initial)	y_initial = y_axle;	
				rows_pos++;
				break;
			}
		}
		*/
		
	}
}
int trfGeneration::DefineCentroid()
{
	int errors = 0;
	vector <vector <int>> connectors_same_intersection;
	vector <vector <int>> connectors_same_centroid;
	vector <vector <int>> save_connectors_same_intersection;
	vector <vector <int>> unsave_connectors_same_intersection;
	for( int i = 0; i < linklist.size(); i++ )  //define the corsim node in database vista_corsim_nodes, if its centroid, save to database centroids and skip!
	{
		int type = linklist[i].returnVistaType();
		if (type == 1) continue;
		int link_id = linklist[i].returnVistaID();
		int upnode = linklist[i].returnVistaOrg();
		int dnnode = linklist[i].returnVistaDest();
		int flag = 0;
		for (int k = 0; k < connectors_same_intersection.size(); k++)
		{
			int node = connectors_same_intersection[k][0];
			if (upnode == node || dnnode == node) 
			{
				flag = -1;
				break;
			}
		}
		if (flag == -1) continue;

		int nodeid = 0;
		for (int k = 0; k < vsta_ndlist.size(); k ++)
		{
			nodeid = vsta_ndlist[k].vsta_ID();
			int nodetype = vsta_ndlist[k].returnVstaType();
			if (nodetype == 1) continue;
			if (nodeid == upnode) 
			{
				nodeid = dnnode;
				break;
			}
			if (nodeid == dnnode) 
			{
				nodeid = upnode;
				break;
			}
		}

		vector <int> inf;
		inf.push_back(nodeid);
		inf.push_back(link_id);
		for( int j = 0; j < linklist.size(); j++ )  //define the corsim node in database vista_corsim_nodes, if its centroid, save to database centroids and skip!
		{
			if (i == j ) continue;
			int type1 = linklist[j].returnVistaType();
			if (type1 == 1) continue;		

			int link_id1 = linklist[j].returnVistaID();
			int upnode1 = linklist[j].returnVistaOrg();
			int dnnode1 = linklist[j].returnVistaDest();
			if ( upnode1 == nodeid ||dnnode1 == nodeid  )inf.push_back(link_id1);			
		}
		connectors_same_intersection.push_back(inf);
	}
	 cout<< "print connectors from the same intersection \n" <<endl;
	for (int i = 0; i < connectors_same_intersection.size(); i ++)
	{
		for (int j= 0; j < connectors_same_intersection[i].size(); j ++)
		{
			cout<<  connectors_same_intersection[i][j] <<"\t";
		}	
		cout<<endl;
	}

	//build the connector vectors which has the same centroid 
	for (int i = 0; i < vsta_ndlist.size(); i ++)
	{
		int nodeid = vsta_ndlist[i].vsta_ID();
		int nodetype = vsta_ndlist[i].returnVstaType();
		if (nodetype == 1) continue;
		vector <int> inf;
		inf.push_back(nodeid);
		for( int j = 0; j < linklist.size(); j++ )  //define the corsim node in database vista_corsim_nodes, if its centroid, save to database centroids and skip!
		{
			int linktype = linklist[j].returnVistaType();
			if (linktype == 1) continue;		

			int link_id = linklist[j].returnVistaID();
			int upnode = linklist[j].returnVistaOrg();
			int dnnode = linklist[j].returnVistaDest();
			if ( upnode == nodeid ||dnnode == nodeid )
			{
				inf.push_back(link_id );
			}
		}
		connectors_same_centroid.push_back(inf);
	}
	cout<< "print connectors from the same centroid \n" <<endl;
	for (int i = 0; i < connectors_same_centroid.size(); i ++)
	{
		for (int j= 0; j < connectors_same_centroid[i].size(); j ++)
		{
			cout<<  connectors_same_centroid[i][j] <<"\t";
		}	
		cout<<endl;
	}
	for (int i = 0; i < connectors_same_intersection.size(); i ++)
	{
		cout<< i <<endl;
		if (connectors_same_intersection[i][0] == 6322 )
			int abc = 123;
		if (connectors_same_intersection[i].size() - 1 < 2) 
		{
			save_connectors_same_intersection.push_back(connectors_same_intersection[i]);
			continue;
		}
		if (connectors_same_intersection[i].size() - 1 == 2) 
		{
			int linkid1= connectors_same_intersection[i][1];
			int linkid2= connectors_same_intersection[i][2];
			int org1 = 0;
			int dest1 = 0;
			int org2=0;
			int dest2 = 0;
			for (int j = 0; j < vista_connectors_nodes.size(); j ++)
			{
				if (linkid1 == vista_connectors_nodes[j][0])
				{
					org1 =  vista_connectors_nodes[j][1];
					dest1 =  vista_connectors_nodes[j][2];
				}
				if (linkid2 == vista_connectors_nodes[j][0])
				{
					org2 =  vista_connectors_nodes[j][1];
					dest2 =  vista_connectors_nodes[j][2];
				}
			}
			if (org1 == org2 || dest1==dest2)
			{
				vector <int> inf;
				inf.push_back(connectors_same_intersection[i][0]);
				inf.push_back(connectors_same_intersection[i][2]);
				save_connectors_same_intersection.push_back(inf);
			}
			else
			{
				save_connectors_same_intersection.push_back(connectors_same_intersection[i]);
			}				
			continue;
		}
		vector <int> inf;
		inf.push_back(connectors_same_intersection[i][0]);
		for (int j= 1; j < connectors_same_intersection[i].size(); j ++)
		{
			int linkid = connectors_same_intersection[i][j];
			for (int k= 0; k < connectors_same_centroid.size(); k ++)
			{
				if (connectors_same_centroid[k].end() == find( connectors_same_centroid[k].begin(),  connectors_same_centroid[k].end(), linkid )) continue;
				if (connectors_same_centroid[k].size()-1 == 1)
				{
					inf.push_back(linkid);
					break;
				}
			}	
			if (inf.size()-1 == 2)break;
		}
		save_connectors_same_intersection.push_back(inf);
	}
	//build unsave_connectors_same_intersection 
	for (int i= 0; i < connectors_same_intersection.size(); i ++)
	{
		if ( connectors_same_intersection[i].size() ==  save_connectors_same_intersection[i].size()) continue;

		int intersection = connectors_same_intersection[i][0];
		vector <int> inf;
		inf.push_back(intersection);
		for (int j= 1; j < connectors_same_intersection[i].size(); j ++)
		{
			int connector =  connectors_same_intersection[i][j];
			if (save_connectors_same_intersection[i].end() != find( save_connectors_same_intersection[i].begin(), save_connectors_same_intersection[i].end(), connector )) continue;

			inf.push_back(connector);
		}

		unsave_connectors_same_intersection.push_back(inf);
	}
				
	int entering_id = 8001;

	for (int i= 0; i < save_connectors_same_intersection.size(); i ++)
	{
		save_connectors_same_intersection[i].push_back(entering_id);
		entering_id++;
	}
	for (int i= 0; i < unsave_connectors_same_intersection.size(); i ++)
	{
		unsave_connectors_same_intersection[i].push_back(entering_id);
		entering_id++;
	}

	cout<< "new i \n" <<endl;
	for (int i= 0; i < connectors_same_centroid.size(); i ++)
	{
		cout<< i <<endl;
		if (i == 57)
			int abc = 123;
		int vista_centroid =  connectors_same_centroid[i][0];
		vector <vector <int>> num_centroids;  // 0 = corsim node 1 = type
		for (int j= 1; j < connectors_same_centroid[i].size(); j ++)
		{
			int linkid = connectors_same_centroid[i][j];
			vector <int> inf;
			for (int k= 0; k < save_connectors_same_intersection.size(); k ++)
			{
				if (save_connectors_same_intersection[k].end()!= find (save_connectors_same_intersection[k].begin() , save_connectors_same_intersection[k].end(), linkid ))
				{
					int corsim_centroid = save_connectors_same_intersection[k][save_connectors_same_intersection[k].size()-1];
					inf.push_back(corsim_centroid);
					inf.push_back(100);
					num_centroids.push_back(inf);
					break;
				}
			}
			for (int k= 0; k < unsave_connectors_same_intersection.size(); k ++)
			{
				if (unsave_connectors_same_intersection[k].end()!= find (unsave_connectors_same_intersection[k].begin() , unsave_connectors_same_intersection[k].end(), linkid ))
				{
					int corsim_centroid = unsave_connectors_same_intersection[k][unsave_connectors_same_intersection[k].size()-1];
					inf.push_back(corsim_centroid);
					inf.push_back(-1);
					num_centroids.push_back(inf);
					break;
				}
			}
		}
		for (int k = 0; k < vsta_ndlist.size(); k ++)
		{
			int id = vsta_ndlist[k].vsta_ID(); 
			if( id ==vista_centroid) 
			{
				for (int j = 0; j < num_centroids.size(); j ++)
				{
					int corsim_centroid = num_centroids[j][0];
					int type = num_centroids[j][1];
					if (j == 0)
					{
						vsta_ndlist[k].corsim_position(corsim_centroid , x_initial, y_initial);			
						vsta_ndlist[k].newType(type);
						continue;
					}
					node nd;
					nd = vsta_ndlist[k];
					nd.corsim_position(corsim_centroid , x_initial, y_initial);			
					nd.newType(type);	
					vsta_ndlist.push_back(nd);
				}	
				break;
			}
		}
	}

	
	for (int i = 0; i < linklist.size(); i ++)
	{
		int type = linklist[i].returnVistaType();
		if (type == 1) continue;
		int linkid = linklist[i].returnVistaID();
		int upnode = linklist[i].returnVistaOrg();
		int dnnode = linklist[i].returnVistaDest();
		int flag = -1;
		int corsim_centroid = 0;
		std::vector<int>::iterator order ;
		for (int j = 0; j <  save_connectors_same_intersection.size(); j ++)
		{
			order = find( save_connectors_same_intersection[j].begin(),save_connectors_same_intersection[j].end(), linkid );
			if (order != save_connectors_same_intersection[j].end()) 
			{	
				flag = 0; 
				int vista_intersection = save_connectors_same_intersection[j][0];
				corsim_centroid =  save_connectors_same_intersection[j][save_connectors_same_intersection[j].size()-1];
				if (vista_intersection == upnode) 
					linklist[i].inputCorsimDn(corsim_centroid);
				if (vista_intersection == dnnode) 
					linklist[i].inputCorsimUp(corsim_centroid);
				linklist[i].newType(100);	
				break;
			}
		}
		if (flag == -1)
		{
			for (int j = 0; j <  unsave_connectors_same_intersection.size(); j ++)
			{
				order = find( unsave_connectors_same_intersection[j].begin(),unsave_connectors_same_intersection[j].end(), linkid );
				if (order != unsave_connectors_same_intersection[j].end()) 
				{	
					int vista_intersection = unsave_connectors_same_intersection[j][0];
					corsim_centroid =  unsave_connectors_same_intersection[j][unsave_connectors_same_intersection[j].size()-1];
					if (vista_intersection == upnode) linklist[i].inputCorsimDn(corsim_centroid);
					if (vista_intersection == dnnode) linklist[i].inputCorsimUp(corsim_centroid);
					linklist[i].newType(-1);	
					break;
				}
			}			
		}
	}
	int crsm_id = 1;
	ofstream node_output1 ("D:\\Amber\\\corsim_test\\output\\node_output1.xls"); //output trf content into text file (for test)
	node_output1<< "node id \t corsim id \t type \n " <<endl; 
	for (int i = 0; i < vsta_ndlist.size(); i ++)
	{
		int id = vsta_ndlist[i].vsta_ID(); 
		int corsim_id = vsta_ndlist[i].crsm_ID();
		int type = vsta_ndlist[i].returnVstaType() ;
		if( type == 1) //find centroids
		{
			vsta_ndlist[i].corsim_position(crsm_id, x_initial, y_initial);
			crsm_id ++;
		}
		int corsim_node =  vsta_ndlist[i].crsm_ID();
		node_output1<<id <<" \t"<< corsim_node  <<" \t"<<  type <<"\n"; 
	}
	node_output1<<endl;

	ofstream link_output1 ("D:\\Amber\\\corsim_test\\output\\link_output1.xls"); //output trf content into text file (for test)
	link_output1<< "link id \t vista upnode \t vista dnnode \t  corsim upnode \t  corsim dnnode \t  type \n " <<endl; 
	for (int i = 0; i <linklist.size(); i ++)
	{
		int id = linklist[i].returnVistaID();
		int vista_up = linklist[i].returnVistaOrg();
		int vista_dn = linklist[i].returnVistaDest();
		int corsim_up =linklist[i].returnCorsimUp() ;
		int corsim_dn = linklist[i].returnCorsimDn() ;
		int type = linklist[i].returnVistaType();
		for (int j = 0; j < vsta_ndlist.size(); j ++)
		{
			int corsim_id =  vsta_ndlist[j].crsm_ID();
			int vista_id = vsta_ndlist[j].vsta_ID();
			if (vista_id == vista_up && corsim_up == 0) 
				corsim_up = corsim_id ;
			if (vista_id == vista_dn && corsim_dn == 0)  
				corsim_dn = corsim_id ;
			if (corsim_up != 0 && corsim_dn != 0) 
				break;
		}
		linklist[i].inputCorsimUp(corsim_up);
		linklist[i].inputCorsimDn(corsim_dn);
		link_output1<<id <<" \t"<< vista_up <<" \t"<<  vista_dn <<" \t"<<  corsim_up <<" \t"<<  corsim_dn <<" \t"<<  type <<"\n"; 
	}
	link_output1<<endl;

	cout<< "print centroids with vista and corsim \n" <<endl;
	for (int i = 0; i < connectors_same_centroid.size(); i ++)
	{
		for (int j= 0; j <connectors_same_centroid[i].size(); j ++)
		{
			cout<<  connectors_same_centroid[i][j] <<"\t";			
		}
		cout<<  endl;
	}	

	return 0;

}

int trfGeneration::CheckNetwork()
{
	//check the connectors connect to the intersection or not
	int errors = 0;
	vector <vector <int>> no_entering;
	vector <vector <int>> no_leaving;
	//define conntectors: more than 1 downstreams or not 
	for( int i = 0; i < linklist.size(); i++ )  
	{
		int type = linklist[i].returnVistaType();
		if (type == -1) continue;
		int corsim_up = linklist[i].returnCorsimUp();
		int corsim_dn = linklist[i].returnCorsimDn();
		int vista_up = linklist[i].returnVistaOrg();
		int vista_dn = linklist[i].returnVistaDest();
		if (corsim_up < 8000) continue;
		int link_id = linklist[i].returnVistaID();
		int nums = 0;
		vector <int> leaving_conntector;
		for( int j = 0; j < linklist.size(); j++ )  //define the corsim node in database vista_corsim_nodes, if its centroid, save to database centroids and skip!
		{
			int type1 = linklist[j].returnVistaType();
			if (type1 == -1 || i == j ) continue;
			int link_id1 = linklist[j].returnVistaID();
			int corsim_up1 = linklist[j].returnCorsimUp();
			int corsim_dn1 = linklist[j].returnCorsimDn();
			int vista_up1 = linklist[j].returnVistaOrg();
			int vista_dn1 = linklist[j].returnVistaDest();

			if (corsim_dn == corsim_up1 )
			{
				if ( corsim_dn1 > 8000 )
				{	
					leaving_conntector.push_back(link_id1);
					leaving_conntector.push_back(vista_up1);
					leaving_conntector.push_back(vista_dn1);
					leaving_conntector.push_back(corsim_up1);
					leaving_conntector.push_back(corsim_dn1);
					continue;
				}
				nums++;
			}
		}
		if (nums > 2)
		{
			int difference = vista_up - vista_dn; 
			if (difference == 150000 || difference == -150000) continue;
			else
			{
				cout<< "link id    "<<link_id <<"   more than two downstreams" <<endl;
				errors++;
			}
		}

	}



	vector <link> lk_entering;
	vector <link> lk_leaving;
	for( int i = 0; i < linklist.size(); i++ )  //define the corsim node in database vista_corsim_nodes, if its centroid, save to database centroids and skip!
	{
		int type = linklist[i].returnVistaType();
		if (type == -1) continue;
		int corsim_up = linklist[i].returnCorsimUp();
		int corsim_dn = linklist[i].returnCorsimDn();
		int vista_up = linklist[i].returnVistaOrg();
		int vista_dn = linklist[i].returnVistaDest();
		int link_id = linklist[i].returnVistaID();
		if (link_id ==18170)
			int abc =123 ;
		link lk_enter;
		link lk_leave;

		int enter_nums = 0;
		int leave_nums = 0;
		int lk_enter_flag = 0;
		int lk_leave_flag = 0;
		for( int j = 0; j < linklist.size(); j++ )  //define the corsim node in database vista_corsim_nodes, if its centroid, save to database centroids and skip!
		{
			int type1 = linklist[j].returnVistaType();
			if (type1 == -1 || i == j ) continue;
			int link_id1 = linklist[j].returnVistaID();
			if (link_id1 == 363506)
				int abc =123 ;
			int corsim_up1 = linklist[j].returnCorsimUp();
			int corsim_dn1 = linklist[j].returnCorsimDn();
			int vista_up1 = linklist[j].returnVistaOrg();
			int vista_dn1 = linklist[j].returnVistaDest();
			if (corsim_dn == corsim_up1  && corsim_up != corsim_dn1)
			{
				leave_nums++;
			}
			if (corsim_up == corsim_dn1 && corsim_dn != corsim_up1)
			{
				enter_nums++;
			}
			if (corsim_dn == corsim_dn1 && type1 == 100)
			{
				lk_leave_flag = 1;
				lk_leave = linklist[j];

			}
			if (corsim_up == corsim_up1 && type1 == 100)
			{
				lk_enter_flag = 1;
				lk_enter = linklist[j];
			}
		}
		if (leave_nums == 0 && type != 100)
		{
			cout<< "link id    "<<link_id <<"   does not have leaving connector" <<endl;
			errors++;
			/*
			int link_id3 = lk.returnVistaID()+ 1000000;
			int corsim_up3 = lk.returnCorsimDn();  
			int corsim_dn3 = lk.returnCorsimUp();
			int vista_up3 = lk.returnVistaDest();
			int vista_dn3 = lk.returnVistaOrg();
			*/
			if (lk_leave_flag == 1)
			{
				cout<< "link id    "<<link_id <<"  will have leaving connector" <<endl;
				lk_leaving.push_back(lk_leave);
			}
			else
			{
				vector <int> ing;
				ing.push_back(link_id);
				ing.push_back(corsim_up);
				ing.push_back(corsim_dn);
				no_leaving.push_back(ing);		
				cout<< "link id    "<<link_id <<"  need to double check" <<endl;
			}
			/*
			cout<< "new linkid = "<<link_id3<<"vista up \t" << vista_up3 <<"vista dn \t" << vista_dn3 <<"corsim up \t" << corsim_up3 <<"corsim dn \t" << corsim_dn3 <<endl;
			
			lk.details(link_id3, lk.returnVistaType(), vista_up3, vista_dn3, lk.returnVistaLength(), lk.returnVistaSpeed(), lk.returnVistaCapacity(), lk.returnVistaLanes());
			lk.inputCorsimDn(corsim_dn3);
			lk.inputCorsimUp(corsim_up3);
			linklist.push_back(lk);
			*/
		}
		if (leave_nums == 0 && type == 100 && corsim_dn < 8000)
		{
			cout<< "link id    "<<link_id <<"   does not have leaving link" <<endl;
			errors++;
		}
		if (enter_nums == 0 && type != 100)
		{
			cout<< "link id    "<<link_id <<"   does not have entering connector" <<endl;
			if(lk_enter_flag == 1)
			{
				lk_entering.push_back(lk_enter);
				cout<< "link id    "<<link_id <<"  will have entering connector" <<endl;
			}
			else
			{
				vector <int> ing;
				ing.push_back(link_id);
				ing.push_back(corsim_up);
				ing.push_back(corsim_dn);
				no_entering.push_back(ing);
				cout<< "link id    "<<link_id <<"  need to double check" <<endl;
			}
			errors++;
		}
	}

	for (int i = 0; i < lk_entering.size(); i ++)
	{
		int link_id =  lk_entering[i].returnVistaID();
		if (link_id == 0) continue;
		link_id =  link_id +1000000;
		int corsim_up = lk_entering[i].returnCorsimDn();
		int corsim_dn = lk_entering[i].returnCorsimUp();
		int vista_up = lk_entering[i].returnVistaDest();
		int vista_dn = lk_entering[i].returnVistaOrg();
		
		cout<< "new entering connector = "<<link_id<<"vista up \t" << vista_up <<"vista dn \t" << vista_dn <<"corsim up \t" << corsim_up <<"corsim dn \n" << corsim_dn <<endl;
		link lk;
		lk.details(link_id, lk_entering[i].returnVistaType(), vista_up, vista_dn, lk_entering[i].returnVistaLength(), lk_entering[i].returnVistaSpeed(), lk_entering[i].returnVistaCapacity(), lk_entering[i].returnVistaLanes());
		lk.inputCorsimDn(corsim_dn);
		lk.inputCorsimUp(corsim_up);
		linklist.push_back(lk);
	}
	for (int i = 0; i < lk_leaving.size(); i ++)
	{
		int link_id =  lk_leaving[i].returnVistaID();
		if (link_id == 0) continue;
		link_id =  link_id +1000000;
		int corsim_up = lk_leaving[i].returnCorsimDn();
		int corsim_dn = lk_leaving[i].returnCorsimUp();
		int vista_up = lk_leaving[i].returnVistaDest();
		int vista_dn = lk_leaving[i].returnVistaOrg();
		
		cout<< "new leaving link = "<<link_id<<"vista up \t" << vista_up <<"vista dn \t" << vista_dn <<"corsim up \t" << corsim_up <<"corsim dn \t" << corsim_dn <<endl;
		link lk;
		lk.details(link_id, lk.returnVistaType(), vista_up, vista_dn, lk.returnVistaLength(), lk.returnVistaSpeed(), lk.returnVistaCapacity(), lk.returnVistaLanes());
		lk.inputCorsimDn(corsim_up);
		lk.inputCorsimUp(corsim_dn);
		linklist.push_back(lk);
		
	}

	vector <int> no_centroid;
	for (int i = 0; i < no_entering.size(); i ++)
	{
		int link_id =  no_entering[i][0];
		int corsim_up = no_entering[i][1];
		int corsim_dn = no_entering[i][2];
		int flag = 0;
		for (int j = 0; j < no_leaving.size(); j ++)
		{
			int link_id1 =  no_leaving[j][0];
			int corsim_up1 = no_leaving[j][1];
			int corsim_dn1 = no_leaving[j][2];
			if (corsim_dn ==corsim_up1 )
			{
				flag = 1;
				no_centroid.push_back(link_id);
				no_entering[i][0] = 0;
				no_entering[j][0] = 0;
				cout << "No centroid: entering link id : " << link_id << "leaving link id : "<< link_id1 <<endl;
				break;
			}
		}
		if (flag == 0)
		{
			cout << "No centroid: entering link id : " << link_id <<endl;
		}
	}

	
	for( int i = 0; i < linklist.size(); i++ )  //define the corsim node in database vista_corsim_nodes, if its centroid, save to database centroids and skip!
	{
		int type = linklist[i].returnVistaType();
		if (type == -1) continue;
		int link_id = linklist[i].returnVistaID();
		int corsim_up = linklist[i].returnCorsimUp();
		int corsim_dn = linklist[i].returnCorsimDn();
		int vista_up = linklist[i].returnVistaOrg();
		int vista_dn = linklist[i].returnVistaDest();
		vector <int> inf;
		inf.push_back(link_id );
		inf.push_back(vista_up);
		inf.push_back(vista_dn);
		inf.push_back(corsim_up);
		inf.push_back(corsim_dn);
		vista_links.push_back(inf);

		
	}
	if (errors > 0 ) 
		return -1;
	return 0;

}

int trfGeneration::CheckCentroid1()
{
	int errors = 0;
	vector <vector <int>> connectors;

	for( int i = 0; i < linklist.size(); i++ )  //define the corsim node in database vista_corsim_nodes, if its centroid, save to database centroids and skip!
	{
		int type = linklist[i].returnVistaType();
		if (type == 1) continue;
		int link_id = linklist[i].returnVistaID();
		int upnode = linklist[i].returnVistaOrg();
		int dnnode = linklist[i].returnVistaDest();
		int flag = 0;
		for (int k = 0; k < connectors.size(); k++)
		{
			int node = connectors[k][0];
			if (upnode == node || dnnode == node) 
			{
				flag = -1;
				break;
			}
		}
		if (flag == -1) continue;

		int nodeid = 0;
		for (int k = 0; k < vsta_ndlist.size(); k ++)
		{
			nodeid = vsta_ndlist[k].vsta_ID();
			int nodetype = vsta_ndlist[k].returnVstaType();
			if (nodetype == 1) continue;
			if (nodeid == upnode) 
			{
				nodeid = dnnode;
				break;
			}
			if (nodeid == dnnode) 
			{
				nodeid = upnode;
				break;
			}
		}

		vector <int> inf;
		inf.push_back(nodeid);
		inf.push_back(link_id);
		for( int j = 0; j < linklist.size(); j++ )  //define the corsim node in database vista_corsim_nodes, if its centroid, save to database centroids and skip!
		{
			if (i == j ) continue;
			int type1 = linklist[j].returnVistaType();
			if (type1 == 1) continue;		

			int link_id1 = linklist[j].returnVistaID();
			int upnode1 = linklist[j].returnVistaOrg();
			int dnnode1 = linklist[j].returnVistaDest();
			if ( upnode1 == nodeid ||dnnode1 == nodeid  )inf.push_back(link_id1);			
		}
		connectors.push_back(inf);
	}
	 
	for (int i = 0; i < connectors.size(); i ++)
	{
		for (int j= 0; j < connectors[i].size(); j ++)
		{
			cout<<  connectors[i][j] <<"\t";
		}	
		cout<<endl;
	}
	return 0;

}

void trfGeneration::getLinkOrient()
{
	/*
	 Calculate all the links' angles
	*/
	int size = linklist.size();	
	cout << "size : " <<  size  << "\n"; 
	int id;
	int vista_org;
	int vista_dest;
	int corsim_org;
	int corsim_dest;
	float length;
	float speed;
	int capacity;
	int lanes;
	int type;


	ofstream output ("D:\\Amber\\\corsim_test\\output\\linklist.xls");

	for( lk_pos = linklist.begin(); lk_pos != linklist.end(); lk_pos++)
	{
		if ((*lk_pos).returnVistaType()==-1) continue;  //ignore the connector which has the removed centroid

		id = (*lk_pos).returnVistaID();
		if (id  == 14455)
			int abc = 123;
		type = (*lk_pos).returnVistaType();
		vista_org = (*lk_pos).returnVistaOrg();
		vista_dest = (*lk_pos).returnVistaDest();
		corsim_org = (*lk_pos).returnCorsimUp();
		corsim_dest = (*lk_pos).returnCorsimDn();

		length = (*lk_pos).returnVistaLength();
		speed = (*lk_pos).returnVistaSpeed();
		lanes = (*lk_pos).returnVistaLanes();
		capacity = (*lk_pos).returnVistaCapacity();
		vector <int> org_pos,dest_pos;  //position of org (x, y)
		org_pos.resize(2);
		std::fill(org_pos.begin(), org_pos.end(), 0);
		dest_pos.resize(2);
		std::fill(dest_pos.begin(), dest_pos.end(), 0);

		int orient_x = 0;
		int orient_y = 0;
		if (id == 0)
			int abc = 123;
		for ( nd_pos = vsta_ndlist.begin(); nd_pos< vsta_ndlist.end(); nd_pos++)  //find the position of the link (up and down nodes)
		{
			int corsim_id = (*nd_pos).crsm_ID();
			if ((*nd_pos).crsm_ID()== corsim_org) 
			{
				org_pos[0]= (*nd_pos).crsm_X();
				org_pos[1]= (*nd_pos).crsm_Y();
			}
			if ((*nd_pos).crsm_ID()== corsim_dest) 
			{
				dest_pos[0]= (*nd_pos).crsm_X();
				dest_pos[1]= (*nd_pos).crsm_Y();
			}
			if(org_pos[0]!= 0 && dest_pos[0]!= 0)
			{
				vector <int> orient;
				orient_x = dest_pos[0] - org_pos[0];
				orient_y = dest_pos[1] - org_pos[1];
				orient.push_back(orient_x);
				orient.push_back(orient_y);
				(*lk_pos).orientation(orient);	
				int angle = (*lk_pos).returnAngle();
				break;
			}
		}
		output << id << "\t" <<  type <<"\t" << corsim_org << "\t" <<  corsim_dest << "\t" <<  length << "\t" <<  speed << "\t" <<  capacity << "\t" <<  lanes << "\n"; 
	}
	ofstream output1 ("D:\\Amber\\\corsim_test\\output\\linkorient.xls");

	for( lk_pos = linklist.begin(); lk_pos != linklist.end(); lk_pos++)
	{
		if ((*lk_pos).returnVistaType()==-1) continue;  //ignore the connector which has the removed centroid
		id = (*lk_pos).returnVistaID();
		if (id  == 14455)
			int abc = 123;
		vector <int> orient = (*lk_pos).returnOrient();
		int angle = (*lk_pos).returnAngle();
		cout << id << "\t" <<  orient[0]<<  "\t"<< orient[1]<<"\t"<< angle <<"\n"; 
		output1 << id << "\t" <<  orient[0]<<  "\t"<< orient[1]<<"\t"<< angle <<"\n"; 

	}

}

void trfGeneration::getVistaNetwork()
{
	int network_size = vista_links.size();

	vista_network.resize(network_size);
	for (int i = 0; i < network_size; i ++)
	{
		vista_network[i].resize(network_size);
		for (int j = 0; j < network_size; j ++)
		{
			vista_network[i][j]=0;
			if (i==j) continue;
			if (vista_links[i][4]==vista_links[j][3] && vista_links[i][3]!=vista_links[j][4])	vista_network[i][j]=1;
			if (vista_links[i][4]==vista_links[j][3] && vista_links[i][3] ==vista_links[j][4]) 
			{
				int times = 0;
				for( lk_pos = linklist.begin(); lk_pos != linklist.end(); lk_pos++) //find upstream's orientation
				{
					int linkid = (*lk_pos).returnVistaID();
					if (linkid == vista_links[i][0])
					{
						times++;
						(*lk_pos).Twoways( vista_links[j][0]); //to record the other side of the link 
					}
					else if (linkid == vista_links[j][0])
					{
						times++;	
						(*lk_pos).Twoways( vista_links[i][0]); //to record the other side of the link 
					}
					if (times== 2) break;
				}				
			}
		}
	}
	ofstream output ("D:\\Amber\\\corsim_test\\output\\network.xls");
	for (int i = 0; i < network_size; i ++)
	{
		vector <vector <int>> downstreams;  //0=id, 1=angle, 2 = corsim node
		int uplink_id = vista_links[i][0];
		if ( uplink_id== 14455)
			int abc = 123;

		int uplink_org = vista_links[i][1];
		int uplink_dest = vista_links[i][2];
		//vector <int> upstream_orient;
		float uplink_length;
		int uplink_angle;
		int dn_links = 0;
		for (int j = 0; j < network_size; j ++)  //to find downstream links
		{
			if (vista_network[i][j]==1) 
			{
				//output<< vista_links[i][0]<<"\t"<<vista_links[j][0]<<"\n";
				vector <int> inf;
				inf.push_back(vista_links[j][0]);
				downstreams.push_back(inf);
				dn_links++;
			}
		}
		//cout<< "link \t " << uplink_id<< "\t number\t" << dn_links<<endl;
		
		for( lk_pos = linklist.begin(); lk_pos != linklist.end(); lk_pos++) //find upstream's orientation
		{
			if (uplink_id == (*lk_pos).returnVistaID()) 
			{
				int id = (*lk_pos).returnVistaID();
				//upstream_orient = (*lk_pos).returnOrient();
				uplink_length = (*lk_pos).returnCORSIMLength();
				uplink_angle = (*lk_pos).returnAngle();
				//cout<<"\n"<<angle_cos << "\t" << angle_sin << "\n";
				break;
			}
		}
		int revise_angle = 180 - uplink_angle;
		int size = downstreams.size();	
		for (int j=0; j < size; j ++)  //find downstream's orientation
		{
			for( lk_pos = linklist.begin(); lk_pos != linklist.end(); lk_pos++)
			{
				int id = (*lk_pos).returnVistaID();
				if (id == downstreams[j][0])
				{
					int angle = (*lk_pos).returnAngle();
					int corsim_downnode =  (*lk_pos).returnCorsimDn();
					downstreams[j].push_back(angle);
					downstreams[j].push_back(corsim_downnode);
					/*
					vector <int> downstream_orient = (*lk_pos).returnOrient();
					int orient_x = downstream_orient[0] - upstream_orient[0];
					int orient_y = downstream_orient[1] - upstream_orient[1];
					downstreams[j].push_back(orient_x);
					downstreams[j].push_back(orient_y);
					float downlink_length = (*lk_pos).returnVistaLength();
					float inner_product = float ((upstream_orient[0]*downstream_orient[0] + upstream_orient[1]*downstream_orient[1])/(uplink_length*downlink_length)); 
					output<< uplink_id <<"\t" << downstreams[j][0] << "\t" << inner_product << "\n";
					*/				
					output<< uplink_id <<"\t" << downstreams[j][0] << "\t" << angle << "\n";
					break;
				}
			}
		}
		vector <int> left;
		left.resize(size);
		int angle[2];

		for (int j=0; j < size; j ++) left[j] = 1;
		for (int j=0; j < size; j ++)   //define downstream's left through right
		{
			for (int k = j+1; k < size; k ++)
			{
				angle[0] = downstreams[j][1];
				angle[1] =  downstreams[k][1];
				for (int z = 0; z < 2; z ++) 
				{	
					angle[z]+= revise_angle;					
					if (angle[z] > 360) angle[z] -= 360;
					else if (angle[z] < 0) angle[z] += 360;
				}
				if (size >= 3)
				{
					if (angle[0] >=angle[1])left[k]++;
					else left[j]++;
				}

			}
		}
		if (size == 2)
		{
			int avc = uplink_id;
			if (uplink_id == 6236)
				avc = 123;
			if (angle[0] < angle[1])
			{
				if (downstreams[0][2] > 8000)
				{ left[0]=3; left[1]=2; }
				else if (downstreams[1][2] > 8000)
				{ left[0]=2; left[1]=1; }
				else if (angle[0] < 150 && angle[1] >210) 
				{ left[0]=3; left[1]=1; }
				else if (angle[0] < 150 && angle[1] < 210) 
				{ left[0]=3; left[1]=2;}
				else if (angle[0] > 150 && angle[1] > 210) 
				{ left[0]=2; left[1]=1;}
				else if (angle[0] > 150 && angle[1] < 210) 
				{ left[0]=3; left[1]=2;}
				else
				{ left[0]=3; left[1]=2;}
			}
			else
			{
				if (downstreams[0][2] > 8000)
				{ left[0]=1; left[1]=2; }
				else if (downstreams[1][2] > 8000)
				{ left[1]=1; left[0]=2; }
				else if (angle[1] < 150 && angle[0] >210) 
				{ left[1]=3; left[0]=1; }
				else if (angle[1] < 150 && angle[0] < 210) 
				{ left[1]=3; left[0]=2;}
				else if (angle[1] > 150 && angle[0] > 210) 
				{ left[1]=2; left[0]=1;}
				else if (angle[1] > 150 && angle[0] < 210) 
				{ left[1]=2; left[0]=3;}
				else
				{ left[1]=2; left[0]=3;}
			}
		}

		for (int j=0; j < size; j ++)
		{
			downstreams[j].push_back(left[j]);
			output<< uplink_id <<"\t" << downstreams[j][0] << "\t" << left[j]<< "\n";
		}
		output<<endl;

		for (int j = 0; j < downstreams.size(); j ++)
		{
			for (int k = 0; k < downstreams[j].size(); k++)
			{
				cout<< downstreams[j][k]<<"\t";	
			}
			cout << "\n";
		}
	

		for( lk_pos = linklist.begin(); lk_pos != linklist.end(); lk_pos++) //find upstream's orientation
		{
			if (uplink_id == (*lk_pos).returnVistaID()) 
			{
				int id = (*lk_pos).returnVistaID();
				(*lk_pos).Downstreams(size,downstreams);
				break;
			}
		}
	}

	ofstream output2 ("D:\\Amber\\\corsim_test\\output\\link_nodes.xls");   
	output2<<"uplinks \t downlinks \n";
	for( lk_pos = linklist.begin(); lk_pos != linklist.end(); lk_pos++)
	{
		if ((*lk_pos).returnVistaType()==-1) continue;  //ignore the connector which has the removed centroid
		int id = (*lk_pos).returnVistaID();
		int org = (*lk_pos).returnVistaOrg();
		int dest = (*lk_pos).returnVistaDest();
		vector <int> dns = (*lk_pos).returnDownNodes();
		output2<<id<<"\t";
		for (int i = 0; i <dns.size(); i ++)
		{
			output2<<dns[i]<<"\t";
		}
		output2<<"\n";
	}
	output2<<endl;



}

void trfGeneration::getVISTAPath()
{
	ifstream file ( "D:\\Amber\\\corsim_test\\input\\vehicle_path.csv" ); 
	//int iCols = CC.GetTotalColumns();   // 總列數
	//int iRows = CC.GetTotalRows();    // 總行數
	//int iCurRow = CC.GetCurrentRow(); // 當前所在行號
	//fprintf(stderr, "column: %d, row: %d", iCols, iRows);
	int id;
	int origin;
	int dest;
	int path_size; //=0 unread, =1 read
	int freeflowtt;
	int length;
	string links;

	int rows_pos = 1;
	while ( file.good())	
	{	
		std:: string value;
		getline ( file, value, ',' );
		if (value=="") 
			break;
		id=stoi(value);

		getline ( file, value, ',' );
		origin=stoi(value);
		
		getline ( file, value, ',' );
		dest=stoi(value);

		getline ( file, value, ',' ); //hash 

		getline ( file, value, ',' );
		path_size=stoi(value);

		getline ( file, value, ',' );
		freeflowtt=stoi(value);

		getline ( file, value, ',' );
		length=stoi(value);

		getline ( file, value, ',' ); //issimpath		

		getline ( file, value, '"' );
		getline ( file, value, '"' );
		links=value;
		CString cs(links.c_str());

		path pth;
		pth.replaceLinks(replace_links);
		pth.PathInfo(id, origin, dest, path_size, freeflowtt, length, cs);
		vsta_pathlist.push_back(pth);
		vsta_pathlist.size();
		rows_pos++;
		cout<<"path row \n" << rows_pos<<endl;
	}
	


	ofstream path_list ("D:\\Amber\\\corsim_test\\output\\vista_path_list.xls");
	ofstream path_list1 ("D:\\Amber\\\corsim_test\\output\\corsim_path_list.xls");
	for (path_pos = vsta_pathlist.begin(); path_pos!= vsta_pathlist.end(); path_pos++)
	{
		id = (*path_pos).returnPathID();
		origin=(*path_pos).returnOrg();
		dest=(*path_pos).returnDest();
		path_size=(*path_pos).returnSize();
		freeflowtt=(*path_pos).returnFreeflowtt();
		length=(*path_pos).returnLength();
		links=(*path_pos).returnLinks();
		vector <int> linklist1 = (*path_pos).returnLinklist();
		
		//cout<<id<<"\t"<<origin<<"\t"<<dest<<"\t"<<path_size<<"\t"<<freeflowtt<<"\t"<<length<<"\t"<<links<<"\n";
		 path_list<<id<<"\t";
		//path_list<<id<<"\t"<<origin<<"\t"<<dest<<"\t"<<path_size<<"\t"<<freeflowtt<<"\t"<<length<<"\t";
		//path_list1<<id <<"\t";
		for (int i = 0; i < linklist1.size(); i ++)
		{
			path_list<<linklist1[i]<<"\t";
			for (int j = 0; j < linklist.size() -1; j++)
			{
				int linkid = linklist[j].returnVistaID();
				int corsim_org = linklist[j].returnCorsimUp();
				int corsim_dest = linklist[j].returnCorsimDn();
				if(linkid == linklist1[i])
				{
					path_list1	<<  corsim_dest<< "\t";
					break;
				}
			}
		}
		path_list1<<"\n";
		path_list<<"\n";
	}
		path_list1<<endl;
		path_list<<endl;
}

void trfGeneration::getVolume()
{
	//CSpreadSheet CC("D:\\Amber\\corsim_test\\path_time.xls", "path_time");
	//CStringArray Rows, Column;
	//CString CValue;

	ifstream file ( "D:\\Amber\\\corsim_test\\input\\vehicle_path_time.csv" ); 
	//int iCols = CC.GetTotalColumns();   // 總列數
	//int iRows = CC.GetTotalRows();    // 總行數
	//int iCurRow = CC.GetCurrentRow(); // 當前所在行號
	//fprintf(stderr, "column: %d, row: %d", iCols, iRows);
	
	int rows_pos = 1;
	while ( file.good())
	{	
		int id;
		int type;
		int dta_departure;
		int sim_departure; //=0 unread, =1 read
		int sim_exittime;
		int dta_path;
		int sim_path;
		std:: string value;
		string arrivaltime;
		
		getline( file, value, ',' );
		if (value=="") 
			break;
		if (stoi(value)==FALSE)
			break;
		id=stoi(value);

		getline ( file, value, ',' );
		type=stoi(value);

		getline ( file, value, ',' );
		dta_departure =stoi(value);	

		getline ( file, value, ',' );
		sim_departure=stoi(value);	

		getline ( file, value, ',' );
		sim_exittime=stoi(value);	

		getline ( file, value, ',' );
		dta_path=stoi(value);	

		getline ( file, value, ',' );
		sim_path=stoi(value);	
		
		getline ( file, value, '"' );
		getline ( file, value, '"' );
		arrivaltime=value;
		CString cs(arrivaltime.c_str());


		for (path_pos = vsta_pathlist.begin(); path_pos!= vsta_pathlist.end(); path_pos++)
		{
			int path_id = (*path_pos).returnPathID();
			if (path_id == dta_path) 
			{
				(*path_pos).trackings( id,  type,  dta_departure,  sim_departure,  sim_exittime,  dta_path,  sim_path,  cs);
				break;
			}
		}
		cout<<rows_pos<<endl;
		rows_pos++;
	}
	
	ofstream path_times ("D:\\Amber\\\corsim_test\\output\\path_vehs.xls");
	for (path_pos = vsta_pathlist.begin(); path_pos!= vsta_pathlist.end(); path_pos++)
	{
		int path_id = (*path_pos).returnPathID();
		if (path_id ==29009) 
			int abc=123;
		vector <int> VehIDs = (*path_pos).returnVehIDs();
		cout<< path_id << "\t" << VehIDs.size() << "\n";
		path_times << path_id << "\t" << VehIDs.size() << "\n";
		/*
		vector <vector <int>> VehTimes=(*path_pos).returnVehTimes();
		for (int i = 0; i <VehIDs.size(); i++)
		{
			path_times<< VehIDs[i] <<"\t";
			for (int j =0; j < VehTimes[i].size(); j ++)
			{
				path_times<< VehTimes[i][j] <<"\t";					
			}
			path_times<< "\n";	
		}
		*/
	}
	path_times<<endl;
	for (path_pos = vsta_pathlist.begin(); path_pos!= vsta_pathlist.end(); path_pos++)
	{
		int pathid = (*path_pos).returnPathID();
		if (pathid == 76) 
			int abc = 123;
		vector <int> links = (*path_pos).returnLinklist();
		vector <int> vehs = (*path_pos).returnVehIDs();
		vector <vector <int>>vehtimes = (*path_pos).returnVehTimes();
		cout <<pathid <<endl;
		for (int i = 0; i < links.size() - 1; i ++)
		{
			int upstream = links[i];
			int dnstream = links[i+1];
			if (upstream ==6284)
				int abc = 123;
			if (dnstream == 6167)
				int abc = 123;
			for (lk_pos = linklist.begin(); lk_pos!= linklist.end(); lk_pos++)
			{
				int linkid = (*lk_pos).returnVistaID();

				if (linkid ==upstream || (i+1 == links.size() - 1 && linkid ==dnstream))
				{ 

					if (upstream== 18394)
						int abc=123;
					for (int j = 0; j < vehs.size(); j ++)
					{
						vector <int> inf;
						inf.push_back(dnstream);
						inf.push_back(vehs[j]);
						inf.push_back(vehtimes[j][i+1]);
						(*lk_pos).DnsVehs(inf);
					}
				}		
			}			
		}
	}
	for (int i = 0; i < vsta_pathlist.size(); i ++)
	{
		int pathid = vsta_pathlist[i].returnPathID();
		if (pathid == 76)
			int abc = 123;
		vector <int> linkorder = vsta_pathlist[i].returnLinklist();
		cout<< pathid <<"\t";
		for (int j = 0; j < linkorder.size(); j ++)
		{
			cout<<linkorder[j]<<"\t";
		}
		cout<<endl;
	}
	ofstream linkflow ("D:\\Amber\\\corsim_test\\output\\linkflow.xls");
	for (int i = 0 ; i < linklist.size(); i ++)
	{
		vector <vector <int> > flows = linklist[i].returnVehs();
		int size = 0;
		for (int j = 0; j < flows.size(); j ++)
		{
			if (flows[j][2] <= 3600) size++;
		}
		linkflow<<linklist[i].returnVistaID()<<"\t"<<size<<"\n";
	}

	linkflow<<endl;

	
}

void trfGeneration::UpstreamNodes()  //
{
	ofstream output ("D:\\Amber\\\corsim_test\\output\\UpstreamNodes.xls");



	for (nd_pos=vsta_ndlist.begin(); nd_pos!= vsta_ndlist.end(); nd_pos++)
	{
		if ( (*nd_pos).returnVstaType()==-1) continue;
		int vsta_node = (*nd_pos).vsta_ID();
		int crsm_node = (*nd_pos).crsm_ID();
		if (crsm_node == 8005) 
			int abc = 123;

		int test = 0;
		for (int i = 0; i < total_corsim_nodes.size(); i++)
		{
			if (total_corsim_nodes[i] == crsm_node) 
			{
				test = 1;
				break;
			}
		}
		if (test == 0) total_corsim_nodes.push_back(crsm_node);
		if (test==1) continue;
		if (crsm_node > 7999) continue;
		
		vector <int> upnodes;
		for (int j = 0; j < vista_links.size(); j ++ )
		{
			int upnode = vista_links[j][3]; //corsim up node
			int dnnode = vista_links[j][4]; 
			if (crsm_node == dnnode) upnodes.push_back(upnode);
		}	
		(*nd_pos).UpNodes(upnodes);

		output << crsm_node<<"\t";
		for (int i = 0; i < upnodes.size(); i ++) output<<upnodes[i] <<"\t";
		output<<endl;
	}
}

int trfGeneration::Signals()
{
	ifstream file ( "D:\\Amber\\\corsim_test\\input\\phases.csv" ); 
	//int iCols = CC.GetTotalColumns();   // 總列數
	//int iRows = CC.GetTotalRows();    // 總行數
	//int iCurRow = CC.GetCurrentRow(); // 當前所在行號
	//fprintf(stderr, "column: %d, row: %d", iCols, iRows);
	
	int rows_pos = 1;
	while ( file.good())
	{	
		int id;
		int type;
		int nodeid;
		int phase; 
		int timered;
		int timeyellow;
		int timegreen;
		int nummoves;
		std:: string linkfrom;
		std:: string linkto;

		string value;	
		getline( file, value, ',' );
		if (value=="") 
			break;	

		id=stoi(value);
		if (id == 5727)
			int abc = 123;

		getline ( file, value, ',' );
		type=stoi(value);

		getline ( file, value, ',' );
		nodeid =stoi(value);	

		getline ( file, value, ',' );

		getline ( file, value, ',' );
		phase=stoi(value);	

		getline ( file, value, ',' );
		timered=stoi(value);	

		getline ( file, value, ',' );
		timeyellow=stoi(value);	

		getline ( file, value, ',' );
		timegreen=stoi(value);	

		getline ( file, value, ',' );
		nummoves=stoi(value);	

		getline ( file, value, '"' );
		getline ( file, value, '"' );
		linkfrom=value;
		CString cs(linkfrom.c_str());

		getline ( file, value, '"' );
		getline ( file, value, '"' );
		linkto=value;
		CString vs(linkto.c_str());
		int flag = 0;
		//int i =  signallist.size() - 1;
		
		for (int i = 0; i < vsta_ndlist.size(); i ++)
		{
			int list_nodeid = vsta_ndlist[i].vsta_ID();
			if (nodeid == list_nodeid) 
			{
				cout<< "node id    "<<   nodeid<< "    signal id  "<< id <<"   pahse" << phase<<endl;
				vsta_ndlist[i].input(id, phase, nodeid, timered, timeyellow, timegreen, cs,vs);
				break;
			}
		
		}		
		rows_pos++;
	}
	int error = 0;
	for (int i = 0 ;i < vsta_ndlist.size(); i++)
	{
		int nodeid =  vsta_ndlist[i].vsta_ID();	
		int signalid = vsta_ndlist[i].returnSignalID();
		int corsim_id = vsta_ndlist[i].crsm_ID();
		if (signalid == 0) continue;
		if (corsim_id == 29)
			int abc = 123;
		cout<< "node id     " <<nodeid<< "  signal id " << signalid <<endl;
		//vector <int> phaseorder=(*nd_pos).returnPhaseOrder();
		vector <vector <int>> phasetime = vsta_ndlist[i].returnPhaseTime();
		vector <vector <int>> linkfrom = vsta_ndlist[i].returnLinkFrom();
		vector <vector <int>> linkto = vsta_ndlist[i].returnLinkTo();
		if (phasetime.size()> 4) 
		{
			cout<<"node id " << nodeid << "   has more than 4 phases!! <<\n"<<endl;
			error = -1;
			continue;
		}
		vector <vector <int>> link_from_to; // 0  phase order, 1 link from, 2 link to
		vector <int> inf;
		std::vector<int>::iterator it;
		for (int i = 0; i < linkfrom.size(); i ++)
		{
			int upstream = 0;
			for (int j = 0; j < linkfrom[i].size(); j ++)
			{
				if ( upstream!= linkfrom[i][j])
				{
					if (inf.size()!= 0) 
					{
						link_from_to.push_back(inf);
						inf.clear();
					}
					inf.push_back(i);
					inf.push_back(linkfrom[i][j]);
					inf.push_back(linkto[i][j]);
					upstream = linkfrom[i][j];
				}
				else
				{
					inf.push_back(linkto[i][j]);
					if (i == linkfrom.size() - 1 && j == linkfrom[i].size() - 1)
					{
						link_from_to.push_back(inf);
						inf.clear();
					}
				}
			}
		}
		if (inf.size()!= 0) 
		{
			link_from_to.push_back(inf);
			inf.clear();
		}
		vector <vector <int>> colors;
		
		for (int i = 0; i <  link_from_to.size(); i ++)
		{
		 //	cout<<"node id	"<<nodeid<<"	phase	"<<phaseorder[i] <<"	green	"<<phasetime[i][0]<< "	yellow	"<< phasetime[i][1]<<"	red	"<<phasetime[i][2]<<endl;
			int phase = link_from_to[i][0];			
			int upstream = link_from_to[i][1];
			inf.push_back(phase);
			inf.push_back(upstream);			
			for (lk_pos = linklist.begin();lk_pos != linklist.end(); lk_pos  ++)
			{
				if (upstream == (*lk_pos).returnVistaID())
				{
					vector <int> linklist_downstreams =  (*lk_pos).returnDownstreams();  //link list: downstream links from the upstream link, try to identify the green phase directions	
					int linklist_downstreams_size = 0;
					for (int size1 = 0; size1< linklist_downstreams.size(); size1++) 
					{
						if (linklist_downstreams[size1] > 0) linklist_downstreams_size++;
					}
					if (linklist_downstreams_size == link_from_to[i].size() - 2) inf.push_back(1);
					else 
					{
						//int number_of_links = (link_from_to[i].size() - 1) - linklist_downstreams.size();
						vector <int> prohibit_directions; //0 left 1 through 2 right 3 diag
						for (int order = 0; order < linklist_downstreams.size(); order++)
						{
								std::vector<int>::iterator it;
								it = find (link_from_to[i].begin()+2,link_from_to[i].end(), linklist_downstreams[order]);
								if (it == link_from_to[i].end() ) 
									prohibit_directions.push_back(order);	
						}
						if ( prohibit_directions.size() == 1)
						{
							if (prohibit_directions[0] == 0) inf.push_back(9); //case 9:  green thru and right only
							if (prohibit_directions[0] == 1) inf.push_back(8); //case 8:  green right and left turn only
							if (prohibit_directions[0] == 2 || prohibit_directions[0] == 3 )
							{
									cout<< "weird directions for through and left: link id = " << upstream  << endl;
							}
						}
						else if ( prohibit_directions.size() == 2)
						{
							if (prohibit_directions.end() == find (prohibit_directions.begin(),prohibit_directions.end(), 3))
							{
								cout<< "weird phase color, node id =   " <<nodeid << endl;							
							}
							else if (prohibit_directions.end()!= find (prohibit_directions.begin(),prohibit_directions.end(), 0)) inf.push_back(9); ///case 9:  green thru and right only
							else if (prohibit_directions.end()!= find (prohibit_directions.begin(),prohibit_directions.end(), 1)) inf.push_back(8); //case 8:  green right and left turn only
							else 
							{
								inf.push_back(1);
								cout<< "weird phase color, node id =   " << nodeid << " link id " <<  upstream << "only provides through and turn right"<< endl;
							}
						}
						else if ( prohibit_directions.size() ==3)
						{
							if (prohibit_directions.end()== find (prohibit_directions.begin(),prohibit_directions.end(), 0)) inf.push_back(4); // case 4:  green left turn only
							else if (prohibit_directions.end()== find (prohibit_directions.begin(),prohibit_directions.end(), 1)) inf.push_back(7); // case 7:  green through only
							else if (prohibit_directions.end()== find (prohibit_directions.begin(),prohibit_directions.end(), 2)) inf.push_back(3); // case 3:  green right only							
							else
							{
							    inf.push_back(6); // case 6:  green diagonal only
								cout<< "weird phase color, node id =   " <<nodeid << endl;
							}						
						
						
						}
						else
						{
							cout<< "weird phase color, node id =   " <<nodeid << endl;
							inf.push_back(1); // case 6:  green diagonal only
						}
							
					}
					break;
				}		
			}
				colors.push_back(inf);
				inf.clear();	
		}
		vsta_ndlist[i].RecordPhaseColor(colors);
	}
	for (int i = 0 ;i < vsta_ndlist.size(); i++)
	{
		int nodeid =  vsta_ndlist[i].vsta_ID();	
		int signalid = vsta_ndlist[i].returnSignalID();
		if (signalid == 0) continue;

		vector <vector <int>> colors = vsta_ndlist[i].returnPhaseColor();
		for (int j = 0; j <  colors.size(); j ++)
		{
			for (int k = 0; k <  colors[j].size(); k ++)
			{
				cout<< colors[j][k]<< "\t " ;
			
			}
		
		}

	}
	
	return error;
}

void trfGeneration::Offsets()
{
	ifstream file ( "D:\\Amber\\\corsim_test\\input\\signals.csv" ); 
	//int iCols = CC.GetTotalColumns();   // 總列數
	//int iRows = CC.GetTotalRows();    // 總行數
	//int iCurRow = CC.GetCurrentRow(); // 當前所在行號
	//fprintf(stderr, "column: %d, row: %d", iCols, iRows);
	
	int rows_pos = 1;
	while ( file.good())
	{	
		int nodeid;
		int type;
		int offset;

		string value;	
		getline( file, value, ',' );
		if (value=="") 
			break;	

		nodeid=stoi(value);


		getline ( file, value, ',' );
		type=stoi(value);

		getline ( file, value, '\n');
		offset =stoi(value);	

		for (int i = 0; i < vsta_ndlist.size(); i ++)
		{
			int list_nodeid = vsta_ndlist[i].vsta_ID();
			if (nodeid == list_nodeid) 
			{
				vsta_ndlist[i].offset(offset);
				break;
			}
		
		}		
		rows_pos++;
	}
}

void trfGeneration::ReorganizePhases()
{/*
	for (int i = 0; i < signallist.size(); i ++)
	{
		int corsim_node = signallist[i].returnCorsimNode();
		vector <vector <int>> phasetime=signallist[i].returnPhaseTime();
		vector <int> phaseorder = signallist[i].returnPhaseOrder();
		vector <vector <int>> linkfrom = signallist[i].returnLinkFrom();
		vector <vector <int>> linkto = signallist[i].returnLinkTo(); */
		/*
		vector <int > connected_uplinks;  //find the upstream links and downstream links from link list
		vector <vector <int>> connected_dnlinks;
		for (int j = 0; j < linklist.size(); j ++)
		{
			if (linklist[j].returnCorsimDn()== corsim_node)	
			{
				int linkid = linklist[j].returnVistaID();
				vector <int> dnlinks = linklist[j].returnDownstreams();
				connected_uplinks.push_back(linkid);
				connected_dnlinks.push_back(dnlinks);			
			}
		}	*/
			
	/*	
		vector <vector <int>> unique_linkfrom;
		vector <vector <int>> phase_color;

		for (int j = 0; j < phaseorder.size(); j++)
		{
			int phase=phaseorder[j] - 1;
			vector <int> sig_uplinks;
			vector <int> sig_downlinks;
			vector <int> colors;
			for (int k = 0; k < linkfrom[phase].size(); k++)
			{
				int uplink = linkfrom[phase][k]; 
				sig_downlinks.push_back(linkto[phase][k]);
				if (k== linkfrom[phase].size() - 1 || uplink != linkfrom[phase][k+1])
				{
					sig_uplinks.push_back(uplink);
					sig_downlinks.clear();
				}
				phase_color.push_back(colors);
				unique_linkfrom.push_back(sig_uplinks);
			}	
		}
	} */
}

void trfGeneration::getTRF()
{
	ifstream fin;
	fin.open("D:\\Amber\\\corsim_test\\test.trf", ios::in);

	char my_character ;
	while (!fin.eof() ) 
	{
		fin.get(my_character);
		number_of_chars++;
		pTRF.push_back(my_character);
		cout << my_character;  //print characters
		if (my_character == '\n')
		{
			++number_of_lines;
		}
	}
	cout << "NUMBER OF LINES: " << number_of_lines << " \n" << endl;
	fin.close();

	ofstream newTRF ("D:\\Amber\\\corsim_test\\test1.trf"); //output trf content into text file (for test)
	for (int i = 0; i < pTRF.size() -1; i++)
	{
		newTRF << pTRF[i];
	}	
}

void trfGeneration::Type195_node()
{
	for (int num_char = 0; num_char <vsta_ndlist.size();num_char++ )
	{
		if (vsta_ndlist[ num_char].returnVstaType()==-1) continue;
		for (int j = 0; j < 80; j ++)	TRF_195.push_back(' ');
		TRF_195.push_back('\n');
	}
	for (int num_char = 0; num_char <fake_nodelist.size();num_char++ )
	{
		for (int j = 0; j < 80; j ++)	TRF_21.push_back(' ');
		TRF_21.push_back('\n');
	}

	int i = 0;
	int node[3]; // 0 = id, 1 = x, 2= y
	int pos[3]; //0=id position, 1 = x position, 2=y's position
	vector <int> corsim_nodes;
	for( nd_pos = vsta_ndlist.begin(); nd_pos != vsta_ndlist.end(); nd_pos++)
	{
		if( (*nd_pos).returnVstaType()==-1) continue;
		node[0] = (*nd_pos).crsm_ID();
		node[1] = (*nd_pos).crsm_X();
		node[2] = (*nd_pos).crsm_Y();
		int bool_corsim_node = 0;

		if (node[0] == 8030)
			int abc= 123;
		for (int i = 0; i < corsim_nodes.size(); i ++)
		{
			if (node[0]== corsim_nodes[i]) 
			{
				bool_corsim_node = -1;
				break;
			}
		}
		if (bool_corsim_node == 0) corsim_nodes.push_back(node[0]);
		else continue;

		pos[0] = 3;  //1~4 id
		pos[1] = 11; //7 - 12 X coordinate (1-999999 Feet)
		pos[2] = 19; //15 - 20 Y coordinate (1-999999 Feet)
		for (int order = 0; order < 3; order ++)
		{
			char buffer [16];
			sprintf(buffer, "%d",node[order]);

			int tens = 0;
			while (float(node[order]) / double(pow(10.0,tens))>=10) tens++;
				

			for (int j = 0; j <= tens; j++) 
			{
				TRF_195[i*81 + pos[order]- j]=buffer[tens-j];
			}

		}
		TRF_195[i*81+77]='1';
		TRF_195[i*81+78]='9';
		TRF_195[i*81+79]='5';
		
		i++;
		cout << node[0] <<"\t"<< node[1] << "\t" <<  node[2] << "\n"; 
	}
	for( int num_char = 0; num_char < fake_nodelist.size(); num_char++)
	{
		node[0] = fake_nodelist[num_char].ReturnCrsm_ID();
		node[1] = fake_nodelist[num_char].ReturnCrsm_X();
		node[2] = fake_nodelist[num_char].ReturnCrsm_Y();

		pos[0] = 3;  //1~4 id
		pos[1] = 11; //7 - 12 X coordinate (1-999999 Feet)
		pos[2] = 19; //15 - 20 Y coordinate (1-999999 Feet)
		for (int order = 0; order < 3; order ++)
		{
			char buffer [16];
			sprintf(buffer, "%d",node[order]);

			int tens = 0;
			while (float(node[order]) / double(pow(10.0,tens))>=10) tens++;
				

			for (int j = 0; j <= tens; j++) 
			{
				TRF_195[i*81 + pos[order]- j]=buffer[tens-j];
			}
		}
		TRF_195[i*81+77]='1';
		TRF_195[i*81+78]='9';
		TRF_195[i*81+79]='5';
		
		i++;
		cout << node[0] <<"\t"<< node[1] << "\t" <<  node[2] << "\n"; 
	}


}

void trfGeneration::Type11_Vista_LinkDescription()
{
	int lines1 = 0;
	int linksize = linklist.size();
	for (int num_char = 0; num_char < linklist.size();num_char++ )
	{
		if (linklist[num_char].returnVistaType() == -1) continue;
		if (linklist[num_char].returnCorsimDn()> 7999) continue;
		for (int j = 0; j < 80; j ++)	TRF_11.push_back(' ');
		TRF_11.push_back('\n');
		lines1++;
	}
	for (int num_char = 0; num_char <fake_linklist.size();num_char++ )
	{
		for (int j = 0; j < 80; j ++)	TRF_11.push_back(' ');
		TRF_11.push_back('\n');
	}

	ofstream output1 ("D:\\Amber\\\corsim_test\\output\\link_departure_nodes.xls");
	ofstream output2 ("D:\\Amber\\\corsim_test\\output\\link_corsim_nodes.xls");
	int lines = 0;
	for (int order = 0; order < linklist.size();order++ )
	{
		int id = linklist[order].returnVistaID();
		if (id == 1018064) 
			int abc = 123;
		int corsim_up = linklist[order].returnCorsimUp();
		int corsim_dn = linklist[order].returnCorsimDn();	

		int vista_up = linklist[order].returnVistaOrg();
		int vista_dn = linklist[order].returnVistaDest();	

		if (corsim_up ==63 && corsim_dn == 28) 
			int abc = 123;


		cout<<"link_id   "<< id << "type "<< linklist[order].returnVistaType() << "\n";
		if (linklist[order].returnVistaType() == -1) continue;
		if (linklist[order].returnCorsimDn()> 7999) continue;

		output2<< id <<"\t"<< vista_up<<"\t"<<vista_dn<<"\t"<<corsim_up<<"\t"<<corsim_dn<<"\n";
		cout<<"line number   "<< lines << "\n";
		int opposite = 0;
		int links = linklist[order].returnNumDns();
		int length = linklist[order].returnVistaLength();
		int lanes = linklist[order].returnVistaLanes();
		int speed = linklist[order].returnCorsimSpeed();	
		vector <int> turn = linklist[order].returnDownNodes();
		vector <int> nodes; // upnode, downnode, left, through, right, diag, opposite 
		nodes.resize(turn.size() + 3);
		std::fill(nodes.begin(),nodes.end(),0);	
		nodes[0] = linklist[order].returnCorsimUp();//for corsim upnode, downnode,
		nodes[1] = linklist[order].returnCorsimDn();	

		vector <int> channelized;
		channelized.resize(lanes);
		std::fill(channelized.begin(),channelized.end(),0);	
		if (lanes == 3 && turn[0]!=0 && turn[2]!=0 && turn[1]==0 && turn[3]==0)
		{
			channelized[0]=4;
			channelized[1]=4;
			channelized[2]=1;
		}
		if (lanes == 4 && turn[0]!=0 && turn[2]!=0 && turn[1]==0 && turn[3]==0)
		{
			channelized[0]=4;
			channelized[1]=4;
			channelized[2]=1;
			channelized[3]=1;
		}

		/*
		for( lk_pos = linklist.begin(); lk_pos != linklist.end(); lk_pos++)  //found the corsim node: 0 upnode, 1 downnode, 2 left,3 right, 4 through, 5 diag (opposite = through), 6 opposite node,
		{
			id = (*lk_pos).returnVistaID();
			if (vista_links[order][0] == id) 
			{
				turn = (*lk_pos).returnDownstreams();
				links = (*lk_pos).returnNumDns();
				nodes.resize(turn.size() + 3);
				std::fill(nodes.begin(),nodes.end(),0);				
				length = (*lk_pos).returnVistaLength();
				lanes = (*lk_pos).returnVistaLanes();
				speed = (*lk_pos).returnCorsimSpeed();

				break;
			}
		}*/
		for (int j = 0; j < turn.size(); j ++)//for downstream nodes
		{
			if (turn[j] == 0) continue;
			nodes[2+j] = turn[j];
		}		
		
		int through_opposite_upnode = turn[1];
		int through_opposite_dnnode = nodes[1]; 
		if (through_opposite_upnode < 8000)
		{
			for( lk_pos = linklist.begin(); lk_pos != linklist.end(); lk_pos++)
			{
				if ((*lk_pos).returnCorsimUp() == through_opposite_upnode && (*lk_pos).returnCorsimDn() == through_opposite_dnnode)
				{
					opposite = through_opposite_upnode;
					
					break;
				}
			}
		}
		if (nodes[0] > 7999) 
		{
			length = 0; //entry link does not have the length
			speed = 0;
			opposite = 0;
		}

		nodes[6] = opposite;
		for (int i = 0; i < nodes.size(); i ++)
		{
			output1<<nodes[i]<<"\t";
		}
		output1<<"\n";

		int pos[7];
		pos[0] = 3; pos[1] = 7; pos[2] = 39; pos[3] = 43; pos[4] = 47; pos[5] = 51; pos[6] = 55; 
		for (int order1 = 0; order1 < (sizeof(pos)/sizeof(*pos)); order1++)
		{
			char buffer [16];
			if (nodes[order1] == 0) continue;
			sprintf(buffer, "%d",nodes[order1]);
			int tens = 0;
			while (float(nodes[order1]) / double(pow(10.0,tens))>=10) tens++;
			for (int j = 0; j <= tens; j++) 
			{
				TRF_11[lines*81 + pos[order1]- j]=buffer[tens-j];
			}
		}
		
		int pos1[3];
		pos1[0] = 11; //length
		pos1[1] = 21; //lanes
		pos1[2] = 67; //freeflowspeed		

		int inf[3]; inf[0] = length; inf[1]=lanes; inf[2] = speed;
		for (int order1 = 0; order1 <(sizeof(pos1)/sizeof(*pos1)); order1++)
		{
			char buffer [16];
			if (inf[order1] == 0) continue;
			sprintf(buffer, "%d",inf[order1]);
			int tens = 0;
			while (float(inf[order1]) / double(pow(10.0,tens))>=10) tens++;
			for (int j = 0; j <= tens; j++) 
			{
				TRF_11[lines*81 + pos1[order1]- j]=buffer[tens-j];
			}
		}
		/*
		2   9 - 12 Length of link (0,50-9999 Feet)                                     3 
		3   22 - 22 Number of lanes (1-9 Number of Lanes)                              0
		9   65 - 68 Desired free-flow speed (0,1-65 Miles Per Hour)                                             3		
		*/
		int pos2[7];
		for (int k = 0; k < 7; k ++)
		{
			pos2[k]=29+k;
		}
		for (int order1 = 0; order1 <lanes; order1++)
		{
			char buffer [16];
			if (inf[order1] == 0) continue;
			sprintf(buffer, "%d",channelized[order1]);
			int tens = 0;
			while (float(channelized[order1]) / double(pow(10.0,tens))>=10) tens++;
			for (int j = 0; j <= tens; j++) 
			{
				TRF_11[lines*81 + pos2[order1]- j]=buffer[tens-j];
			}
		}


			TRF_11[lines*81+27]='0';	//27 - 28 Grade (-9-9 Percentage)  
			TRF_11[lines*81+28]='1';    //29 - 29 Distribution Code. Queue discharge and start-up lost time characteristics. (1-4)
		
			TRF_11[lines*81+58]='2';
			TRF_11[lines*81+59]='0';    //57 - 60 Mean value of start-up lost time (0-99 Tenths of Seconds)
			TRF_11[lines*81+62]='1';   	
			TRF_11[lines*81+63]='8';	//61 - 64 Mean queue discharge headway (14-99 Tenths of Seconds)                                      3 

			TRF_11[lines*81+70]='1';	//71 - 71 Pedestrian code (0-3)
		
			TRF_11[lines*81+78]='1';
			TRF_11[lines*81+79]='1';
			
			lines++;
		}
	for (int order = 0; order < fake_linklist.size();order++ )
	{
		int id = fake_linklist[order].returnVistaID();

		int opposite = 0;
		int length = fake_linklist[order].returnCorsimLength();
		int lanes = fake_linklist[order].returnCorsimLanes();
		int speed = fake_linklist[order].returnCorsimSpeed();	
		int turn[4] = {0};
		int nodes[7] = {0}; // upnode, downnode, left, through, right, diag, opposite 

		nodes[0] = fake_linklist[order].returnCorsimUp();//for corsim upnode, downnode,
		nodes[1] = fake_linklist[order].returnCorsimDn();
		nodes[3] = fake_linklist[order].returnCorsimOpst();
		if (nodes[0]  > 7999) 
		{
			length = 0; //entry link does not have the length
			speed = 0;
			opposite = 0;
		}
		nodes[6] = opposite;
		int pos[7];
		pos[0] = 3; pos[1] = 7; pos[2] = 39; pos[3] = 43; pos[4] = 47; pos[5] = 51; pos[6] = 55; 
		for (int order1 = 0; order1 < (sizeof(pos)/sizeof(*pos)); order1++)
		{
			char buffer [16];
			if (nodes[order1] == 0) continue;
			sprintf(buffer, "%d",nodes[order1]);
			int tens = 0;
			while (float(nodes[order1]) / double(pow(10.0,tens))>=10) tens++;
			for (int j = 0; j <= tens; j++) 
			{
				TRF_11[lines*81 + pos[order1]- j]=buffer[tens-j];
			}
		}
		
			/* total position = 7
			0   1 -  4 Link's upstream node number (1-6999,7000-7999,8000-8999 Node ID)    3
			1   5 -  8 Link's downstream node number (1-6999,7000-7999,8000-8999 Node ID)  3
			2   37 - 40 Downstream node receiving left-turning traffic (1-6999,7000-7999,8000-8999 Node ID) 3
			3   41 - 44 Downstream node receiving through traffic (1-6999,7000-7999,8000-8999 Node ID)      3
			4   45 - 48 Downstream node receiving right-turning traffic (1-6999,7000-7999,8000-8999 Node ID)3 
			5   49 - 52 Downstream node receiving diagonal traffic (1-6999,7000-7999,8000-8999 Node ID)     3
			6   53 - 56 Upstream node for traffic opposing left-turning traffic (1-6999,7000-7999,8000-8999 Node ID) 3
			*/
			int pos1[3];
			pos1[0] = 11; //length
			pos1[1] = 21; //lanes
			pos1[2] = 67; //freeflowspeed		

			int inf[3]; inf[0] = length; inf[1]=lanes; inf[2] = speed;
			for (int order1 = 0; order1 <(sizeof(pos1)/sizeof(*pos1)); order1++)
			{
				char buffer [16];
				if (inf[order1] == 0) continue;
				sprintf(buffer, "%d",inf[order1]);
				int tens = 0;
				while (float(inf[order1]) / double(pow(10.0,tens))>=10) tens++;
				for (int j = 0; j <= tens; j++) 
				{
					TRF_11[lines*81 + pos1[order1]- j]=buffer[tens-j];
				}
			}
			/*
			2   9 - 12 Length of link (0,50-9999 Feet)                                     3 
			3   22 - 22 Number of lanes (1-9 Number of Lanes)                              0
			9   65 - 68 Desired free-flow speed (0,1-65 Miles Per Hour)                                             3		
			*/



			TRF_11[lines*81+27]='0';	//27 - 28 Grade (-9-9 Percentage)  
			TRF_11[lines*81+28]='1';    //29 - 29 Distribution Code. Queue discharge and start-up lost time characteristics. (1-4)
		
			TRF_11[lines*81+58]='2';
			TRF_11[lines*81+59]='0';    //57 - 60 Mean value of start-up lost time (0-99 Tenths of Seconds)
			TRF_11[lines*81+62]='1';   	
			TRF_11[lines*81+63]='8';	//61 - 64 Mean queue discharge headway (14-99 Tenths of Seconds)                                      3 

			TRF_11[lines*81+70]='1';	//71 - 71 Pedestrian code (0-3)
		
			TRF_11[lines*81+78]='1';
			TRF_11[lines*81+79]='1';
			
			lines++;
			cout<<lines<<endl;
		}	

}

void trfGeneration::ChangeFakeLinkNodeBack()
{
	for (int i = 0; i < fakenodes.size(); i ++)
	{
		int fake_node_id = fakenodes[i][2];
		int real_node_id = fakenodes[i][0];
		/*
		for (int j = 0; j < linklist.size(); j ++)
		{
			int type = linklist[j].returnVistaType();
			if (type == -1) continue;

			int link_id = linklist[j].returnVistaID();
			int corsim_up = linklist[j].returnCorsimUp();
			int corsim_dn = linklist[j].returnCorsimDn();
			int vista_up = linklist[j].returnVistaOrg();
			int vista_dn = linklist[j].returnVistaDest();
			if( fake_node_id == corsim_up)linklist[j].inputCorsimUp(real_node_id);
			if( fake_node_id == corsim_dn)linklist[j].inputCorsimDn(real_node_id);	
		}*/
		
		for( int j = 0; j < vsta_ndlist.size(); j++ )  //define the corsim node in database vista_corsim_nodes, if its centroid, save to database centroids and skip!
		{		
			int vista_id = vsta_ndlist[j].vsta_ID();
			int corsim_id = vsta_ndlist[j].crsm_ID();
			int corsim_x =  vsta_ndlist[j].crsm_X();
			int corsim_y =  vsta_ndlist[j].crsm_Y();
			int type = vsta_ndlist[j].returnVstaType();
			if( fake_node_id == corsim_id ) vsta_ndlist[j].inputCorsimNode(real_node_id);

		}
		
	}

}

void trfGeneration::Type21_TurnMovement()
{
	for (int num_char = 0; num_char <linklist.size();num_char++ )
	{
		if (linklist[num_char].returnVistaType() == -1) continue;
		if (linklist[num_char].returnCorsimDn()> 7999) continue;

		for (int j = 0; j < 80; j ++)	TRF_21.push_back(' ');
		TRF_21.push_back('\n');
	}

	int i = 0;
	ofstream output ("D:\\Amber\\\corsim_test\\output\\downstream_flow.xls");
	int lines = 0;
	for (lk_pos = linklist.begin(); lk_pos!= linklist.end(); lk_pos++)
	{
		if ((*lk_pos).returnVistaType() == -1) continue;
		if ((*lk_pos).returnCorsimDn()> 7999) continue;

		int up = (*lk_pos).returnCorsimUp();
		int dn=(*lk_pos).returnCorsimDn();
		cout<< "    corsim up  " << up << "    corsim down  " << dn << endl;
		if (up== 8006 && dn == 18)
			int abc = 123;
		vector <vector <int>> linkflow = (*lk_pos).returnLinkFlow();  //0=dn's id, 1 = vehid, 2= time
		vector <int> turns_links = (*lk_pos).returnDownstreams();  //0=left, 1 = through, 2 = right, 3=diag
		vector <int> turns = (*lk_pos).returnDownNodes();		
		vector <int> inf;
		inf.resize(turns.size()+2);
		std::fill(inf.begin(),inf.end(),0);	
		inf[0]=up; 		
		inf[1]=dn;	//0=upnode, 1 = downnode, 2 = left, 3=through, 4=right, 5=diag
		int linkid=(*lk_pos).returnVistaID();
		for (int i = 0; i < linkflow.size(); i ++)
		{
			if (linkflow[i][2] > 3600) continue;
			for (int j = 0; j < turns_links.size(); j ++)
			{
				if(linkflow[i][0]==turns_links[j])
				{
					inf[j+2]++;
					break;
				}
			}
		}
		int flow_sum = 0;
		for (int i = 0; i < turns.size(); i ++) flow_sum +=  inf[i+2]; //sum of traffic by directions

		//if (inf[1] > 7999) inf[3] = 100;  //in[3] is the through flow inf[0] ==upnode

		if (flow_sum== 0) 
		{
			for (int i = 0; i < turns.size(); i ++) 
			{
				if (turns[i] != 0) inf[i+2] = 100;
				//else inf[i+2] = 0;
			}
			flow_sum+= inf[i+2] ;
		}
		int pos[6]; //0=id position, 1 = x position, 2=y's position
		pos[0]= 3; pos[1]= 7; pos[2]=11; pos[3]=15; pos[4]=19; pos[5]=23; 

		for (int order1 = 0; order1 < (sizeof(pos)/sizeof(*pos)); order1++)
		{
			if ((flow_sum == 0 ) && (order1== 2 || order1== 4 || order1== 5 )) continue;

			char buffer [16];
			sprintf(buffer, "%d",inf[order1]);
			int tens = 0;
			while (float(inf[order1]) / double(pow(10.0,tens))>=10) tens++;
			for (int j = 0; j <= tens; j++) 
			{
				TRF_21[lines*81 + pos[order1]- j]=buffer[tens-j];
			}
		}
		
		TRF_21[lines*81+78]='2';
		TRF_21[lines*81+79]='1';
		lines++;
		output<<linkid <<"\t"<<inf[0]<<"\t"<<inf[1]<<"\t"<<inf[2]<<"\t"<<inf[3]<<"\t"<<inf[4]<<"\t"<<inf[5]<<"\n";
	}
	
	for( int num_char = 0; num_char < fake_linklist.size(); num_char++)
	{
		int id = fake_linklist[num_char].returnVistaID();
		vector <vector <int>> linkflow ;  //0=dn's id, 1 = vehid, 2= time
		for (lk_pos = linklist.begin(); lk_pos!= linklist.end(); lk_pos++)
		{
			int linkid = (*lk_pos).returnVistaID();
			if (linkid == id)
			{
				linkflow = (*lk_pos).returnLinkFlow();
				break;
			}
		}
		int up = fake_linklist[num_char].returnCorsimUp();
		int dn= fake_linklist[num_char].returnCorsimDn();
		int opst = fake_linklist[num_char].returnCorsimOpst();
		cout<< "    corsim up  " << up << "    corsim down  " << dn << endl;
		int inf[6] = {0};
		
		inf[0]=up; 		
		inf[1]=dn;	//0=upnode, 1 = downnode, 2 = left, 3=through, 4=right, 5=diag

		int linkid=(*lk_pos).returnVistaID();
		for (int i = 0; i < linkflow.size(); i ++)
		{
			if (linkflow[i][2] > 3600) continue;
			inf[3]++;
		}
		int flow_sum = 0;
		for (int i = 0; i < (sizeof(inf)/sizeof(*inf)); i ++) flow_sum +=  inf[i+2]; //sum of traffic by directions

		if (flow_sum== 0) 
		{
			inf[3] = 100;
		}
		int pos[6]; //0=id position, 1 = x position, 2=y's position
		pos[0]= 3; pos[1]= 7; pos[2]=11; pos[3]=15; pos[4]=19; pos[5]=23; 

		for (int order1 = 0; order1 < (sizeof(pos)/sizeof(*pos)); order1++)
		{
			if ((flow_sum == 0 || inf[0] > 7999) && (order1== 2 || order1== 4 || order1== 5 )) continue;

			char buffer [16];
			sprintf(buffer, "%d",inf[order1]);
			int tens = 0;
			while (float(inf[order1]) / double(pow(10.0,tens))>=10) tens++;
			for (int j = 0; j <= tens; j++) 
			{
				TRF_21[lines*81 + pos[order1]- j]=buffer[tens-j];
			}
		}
		
		TRF_21[lines*81+78]='2';
		TRF_21[lines*81+79]='1';
		lines++;
		output<<linkid <<"\t"<<inf[0]<<"\t"<<inf[1]<<"\t"<<inf[2]<<"\t"<<inf[3]<<"\t"<<inf[4]<<"\t"<<inf[5]<<"\n";
	}
	/*
   Columns:
   1)  1 -  4 Upstream node number (1-6999,7000-7999,8000-8999 Node ID)
   2)  5 -  8 Downstream node number (1-6999,7000-7999 Node ID)
   3)  9 - 12 Left-turning traffic (0-9999 Percentage or Number of Vehicles)
   4) 13 - 16 Through traffic (0-9999 Percentage or Number of Vehicles)
   5) 17 - 20 Right-turning traffic (0-9999 Percentage or Number of Vehicles)
   6) 21 - 24 Diagonal-turning traffic (0-9999 Percentage or Number of Vehicles)
   11) 79 - 80 Record Type (21)	
	*/

}

void trfGeneration::GreenPhase()
{	
	for (int i = 0; i < signallist.size(); i ++)
	{
		int corsim_node = signallist[i].returnCorsimNode();
		vector <vector <int>> phasetime=signallist[i].returnPhaseTime();
		vector <int> phaseorder = signallist[i].returnPhaseOrder();
		vector <vector <int>> linkfrom = signallist[i].returnLinkFrom();
		vector <vector <int>> linkto = signallist[i].returnLinkTo();
		
		vector <int > connected_uplinks;  //find the upstream links and downstream links from link list
		vector <vector <int>> connected_dnlinks;
		for (int j = 0; j < linklist.size(); j ++)
		{
			if (linklist[j].returnCorsimDn()== corsim_node)	
			{
				int linkid = linklist[j].returnVistaID();
				vector <int> dnlinks = linklist[j].returnDownstreams();
				connected_uplinks.push_back(linkid);
				connected_dnlinks.push_back(dnlinks);			
			}
		}	
			
		
		vector <vector <int>> unique_linkfrom;
		vector <vector <int>> phase_color;

		for (int j = 0; j < phaseorder.size(); j++)
		{
			int phase=phaseorder[j] - 1;
			vector <int> sig_uplinks;
			vector <int> sig_downlinks;
			vector <int> colors;
			for (int k = 0; k < linkfrom[phase].size(); k++)
			{
				int uplink = linkfrom[phase][k]; 
				sig_downlinks.push_back(linkto[phase][k]);
				if (k== linkfrom[phase].size() - 1 || uplink != linkfrom[phase][k+1])
				{
					sig_uplinks.push_back(uplink);
					for (int order = 0; order <connected_uplinks.size(); order++)
					{
						if (connected_uplinks[order]== uplink)
						{
							vector <int> prohibit_directions; //0 left 1 through 2 right 3 diag
							for (int d = 0; d <connected_dnlinks[order].size(); d++)
							{
								std::vector<int>::iterator it;
								it = find (sig_downlinks.begin(),sig_downlinks.end(), connected_dnlinks[order][d]);
								if (it ==sig_downlinks.end() ) prohibit_directions.push_back(d);
							}
							if ( prohibit_directions.size() == 0) colors.push_back(1);
							if ( prohibit_directions.size() == 1)
							{
								if (prohibit_directions[0] == 0) colors.push_back(9); //case 9:  green thru and right only
								if (prohibit_directions[0] == 1) colors.push_back(8); //case 8:  green right and left turn only
								if (prohibit_directions[0] == 2)
								{cout<< "weird directions for through and left: link id = " << uplink << endl; }
							}
							if ( prohibit_directions.size() == 2)
							{
								if (sig_downlinks.end()== find (prohibit_directions.begin(),prohibit_directions.end(), 0)) colors.push_back(4); // case 4:  green left turn only
								if (sig_downlinks.end()== find (prohibit_directions.begin(),prohibit_directions.end(), 1)) colors.push_back(7); // case 7:  green through only
								if (sig_downlinks.end()== find (prohibit_directions.begin(),prohibit_directions.end(), 2)) colors.push_back(3); // case 3:  green right turn only
								if (sig_downlinks.end()== find (prohibit_directions.begin(),prohibit_directions.end(), 3)) colors.push_back(6); // case 6:  green diagonal only
							}
						}
					}
					 sig_downlinks.clear();
				}
				phase_color.push_back(colors);
				unique_linkfrom.push_back(sig_uplinks);
			}	
		}
	}
	for (int i = 0; i < signallist.size(); i ++)
	{
		vector<vector<int>> directions = signallist[i].returnUniqueUplinks();
		vector<vector<int>> phasecolors = signallist[i].returnPhaseColor();
		int node_id = signallist[i].returnNodeID(); 
		cout << "node id = "<< node_id<<endl;
		for (int j = 0; j < directions.size(); j ++)
		{
			for (int k = 0; k < directions[j].size(); k ++)
			{
				cout<< "phase  " << j+1 << "uplink  "<<directions[j][k]<<"  color  "<< phasecolors[j][k]<<endl;
			}
		
		}
	}

}

void trfGeneration::Type35_36()
{
	int line1 = 0;
	for (int i = 0; i <vsta_ndlist.size() +5; i ++)
	{
		cout<<i<<endl;
		if (i == 152)
			int abc =123;
		for (int j = 0; j < 80; j ++)	TRF_35.push_back(' ');
		for (int j = 0; j < 80; j ++)	TRF_36.push_back(' ');
		TRF_35.push_back('\n');
		TRF_36.push_back('\n');
		line1++;
	}
	int type35_pos[19];
	type35_pos[0] = 3; type35_pos[1] = 7; type35_pos[2] = 11; type35_pos[3] = 15; type35_pos[4] = 19; type35_pos[5] = 23; type35_pos[6] = 27; type35_pos[7] = 31; type35_pos[8] = 35; type35_pos[9] = 39; type35_pos[10] = 43; 
	type35_pos[11] = 47; type35_pos[12] = 51; type35_pos[13] = 55; type35_pos[14] = 59; type35_pos[15] = 63; type35_pos[16] = 67; type35_pos[17] = 71; type35_pos[18] = 75; 

	int type36_pos[60];
	for (int i = 0; i < 60; i ++) type36_pos[i]=5+i; 

	int lines = 0;
	for (int i = 0; i <vsta_ndlist.size(); i ++)
	{
		if (i == 150)
			int abc =123;
		int vista_nodeid = vsta_ndlist[i].vsta_ID();
		int corsim_nodeid = vsta_ndlist[i].crsm_ID();
		if (vsta_ndlist[i].crsm_ID() > 7999) continue;

		cout<< "Current NODE ID = " << corsim_nodeid << "\n";
		if (corsim_nodeid == 116)
			int abc =123;
		int type35_inf[19] = {0};
		int type36_inf[60];	
		for (int j = 0; j < (sizeof(type36_pos)/sizeof(*type36_pos)); j ++ )	{type36_inf[j] = 100;}
		
		type35_inf[0] = corsim_nodeid;
		//type36_inf[0]=vsta_ndlist[i].crsm_ID();

		vector <int> upnodes= vsta_ndlist[i].returnUpNodes();


		if (upnodes.size() > 5) 
			{cout<< "TOO MANY UPSTREAM NODES! NODE ID = " << type35_inf[0]<< "\n"; }
		if (upnodes.size()==0) continue;
		
		vector <int> link_detail_position;
		for (int j = 0; j <vista_links.size(); j ++)
		{
			int corsim_upnode=  vista_links[j][3];
			int corsim_dnnode=  vista_links[j][4];
			if (corsim_dnnode==  corsim_nodeid) link_detail_position.push_back(j);
		}		
		for (int k = 0; k < upnodes.size(); k ++) type35_inf[k+2]=upnodes[k];
		int signal_flag = vsta_ndlist[i].returnSignalID(); 
		
		if (signal_flag != 0)
		{
			type35_inf[1] = vsta_ndlist[i].returnoffset();   //  Reference offset to signal interval 1 (0-9999 Seconds)
			vector <vector <int>> phasetime = vsta_ndlist[i].returnPhaseTime();
			vector <int> phaseorder  = vsta_ndlist[i].returnPhaseOrder();
			vector <vector <int>> phasecolor = vsta_ndlist[i].returnPhaseColor();
			int order = 0;

			vector <int> upnodes_order;
			vector <int> uplinks_order;
			for (int k = 0; k <upnodes.size(); k ++)
			{
				if (upnodes[k] == 0) continue;
				for (int j = 0; j <link_detail_position.size(); j ++)
				{
					int pos = link_detail_position[j];
					if (upnodes[k]==  vista_links[pos][3])
					{
						upnodes_order.push_back(upnodes[k]);
						uplinks_order.push_back(vista_links[pos][0]);
						break;
					}
				}
			}

			for (int k = 0; k < phasetime.size(); k ++) 
			{
				cout<<k<< "\t"<<i<<endl;
				for (int j = 0; j < phasetime[k].size(); j ++)
				{
					type35_inf[order+7]=phasetime[k][j]; 
					order++;
				}
			}

			for (int k = 0; k < 4; k ++) //12 phases is the maximum in CORSIM (green yellow, red *4) ==> 4 iterations
			{
				int phase = 0;
				for (int j = 0; j < phaseorder.size(); j ++)
				{
					if (phaseorder[j]== k + 1) 
					{	
						phase = j;
						break;
					}
				}
				if (phaseorder[phase] != k + 1) continue;
				for (int j = 0; j < uplinks_order.size(); j ++)
				{
					for (int t = 0; t < phasecolor.size(); t ++)
					{
						if (phasecolor[t][1]!= uplinks_order[j]) continue;
						if (phasecolor[t][0] != phaseorder[phase] - 1)
						{	
							type36_inf[j+ 15*k] = 2;
							type36_inf[j+ 15*k + 5] = 2;
							type36_inf[j+ 15*k + 10] = 2;
						}
						else if (phasecolor[t][0] == phaseorder[phase] - 1 )
						{
							type36_inf[j+ 15*k] = phasecolor[t][2];
							type36_inf[j+ 15*k + 5] = 0;
							type36_inf[j+ 15*k + 10] = 2;
							break;
						}
						else
						{
							cout<<"signal id   " <<  corsim_nodeid << "  is weird!" <<endl;
						}
					
					}
				}

			}
		}
		else
		{
			//for no signal at type 36
			for (int num = 0; num <  upnodes.size(); num++)
			{
				type36_inf[num] = 1;
			}
		}

		for (int order1 = 0; order1 < (sizeof(type36_pos)/sizeof(*type36_pos)); order1++)
		{
			char buffer [16];
			if(type36_inf[order1] ==100) continue;
			sprintf(buffer, "%d",type36_inf[order1]);
			int tens = 0;
			while (float(type36_inf[order1]) / double(pow(10.0,tens))>=10) tens++;
			for (int k = 0; k <= tens; k++) 
			{
				TRF_36[lines*81 + type36_pos[order1]- k]=buffer[tens-k];
			}
		}
		for (int order1 = 0; order1 < (sizeof(type35_pos)/sizeof(*type35_pos)); order1++)
		{
			char buffer [16];
			if(type35_inf[order1] ==0) continue;

			sprintf(buffer, "%d",type35_inf[order1]);
			int tens = 0;
			while (float(type35_inf[order1]) / double(pow(10.0,tens))>=10) tens++;
			for (int k = 0; k <= tens; k++) 
			{
				if (order1 == 0 ) TRF_36[lines*81 + type35_pos[order1]- k]=buffer[tens-k];
				
				TRF_35[lines*81 + type35_pos[order1]- k]=buffer[tens-k];
			}
		}	

		TRF_35[lines*81+78]='3';
		TRF_35[lines*81+79]='5';
		TRF_36[lines*81+78]='3';
		TRF_36[lines*81+79]='6';
		lines++;
		/*
		for (int j = i; j < vsta_ndlist.size(); j ++)
		{
			if (vsta_ndlist[j].crsm_ID() == total_corsim_nodes[i])
			{
				int inf[19] = {0};
				inf[0]=vsta_ndlist[j].crsm_ID();
				inf[1] = 0;   //  Reference offset to signal interval 1 (0-9999 Seconds)
				vector <int> upnodes= vsta_ndlist[j].returnUpNodes();
				if (upnodes.size() > 5) 
				{cout<< "TOO MANY UPSTREAM NODES! NODE ID = " << inf[0]<< "\n"; }
				for (int k = 0; k < upnodes.size(); k ++) inf[k+2]=upnodes[k];

				for (int order1 = 0; order1 < (sizeof(pos)/sizeof(*pos)); order1++)
				{
					char buffer [16];
					if(inf[order1] ==0) continue;

					sprintf(buffer, "%d",inf[order1]);
					int tens = 0;
					while (float(inf[order1]) / double(pow(10.0,tens))>=10) tens++;
					for (int k = 0; k <= tens; k++) 
					{
						TRF_35[lines*81 + pos[order1]- k]=buffer[tens-k];
					}
				}

				TRF_35[lines*81+78]='3';
				TRF_35[lines*81+79]='5';
				lines++;
				break;
			}
		}
	}*/
	/*for (nd_pos=vsta_ndlist.begin(); nd_pos!= vsta_ndlist.end(); nd_pos++)
	{	
		int test = 0;
		for (int i = 0; i < total_corsim_nodes.size(); i ++)
		{
			if ((*nd_pos).crsm_ID() == total_corsim_nodes[i]) 
			{
				total_corsim_nodes[i] = 0;
				test =1;
			}
		}
		if (test==0) continue;
		int inf[19] = {0};
		inf[0]=(*nd_pos).crsm_ID();
		inf[1] = 0;   //  Reference offset to signal interval 1 (0-9999 Seconds)
		vector <int> upnodes= (*nd_pos).returnUpNodes();
		if (upnodes.size() > 5) 
		{cout<< "TOO MANY UPSTREAM NODES! NODE ID = " << inf[0]<< "\n"; }
		for (int i = 0; i < upnodes.size(); i ++) inf[i+2]=upnodes[i];

		for (int order1 = 0; order1 < (sizeof(pos)/sizeof(*pos)); order1++)
		{
			char buffer [16];
			if(inf[order1] ==0) continue;

			sprintf(buffer, "%d",inf[order1]);
			int tens = 0;
			while (float(inf[order1]) / double(pow(10.0,tens))>=10) tens++;
			for (int j = 0; j <= tens; j++) 
			{
				TRF_35[lines*81 + pos[order1]- j]=buffer[tens-j];
			}
		}

		TRF_35[lines*81+78]='3';
		TRF_35[lines*81+79]='5';
		lines++;
	}*/
	
	/* Card Type: 35 (Sign or Pre-timed Signal Control Timing)
		1 -  4 Node number of the intersection (1-6999 Node ID)
		5 -  8 Reference offset to signal interval 1 (0-9999 Seconds)
		9 - 12 Upstream node number of approach link 1 (1-6999,7000-7999,8000-8999 Node ID)
		13 - 16 Upstream node number of approach link 2 (1-6999,7000-7999,8000-8999 Node ID)
		17 - 20 Upstream node number of approach link 3 (1-6999,7000-7999,8000-8999 Node ID)
		21 - 24 Upstream node number of approach link 4 (1-6999,7000-7999,8000-8999 Node ID)
		25 - 28 Upstream node number of approach link 5 (1-6999,7000-7999,8000-8999 Node ID)
		30 - 32 Duration of signal interval 1 (1-120 Seconds)
		34 - 36 Duration of signal interval 2 (1-120 Seconds)
		38 - 40 Duration of signal interval 3 (1-120 Seconds)
		42 - 44 Duration of signal interval 4 (1-120 Seconds)
		46 - 48 Duration of signal interval 5 (1-120 Seconds)
		50 - 52 Duration of signal interval 6 (1-120 Seconds)
		54 - 56 Duration of signal interval 7 (1-120 Seconds)
		58 - 60 Duration of signal interval 8 (1-120 Seconds)
		62 - 64 Duration of signal interval 9 (1-120 Seconds)
		66 - 68 Duration of signal interval 10 (1-120 Seconds)
		70 - 72 Duration of signal interval 11 (1-120 Seconds)
		74 - 76 Duration of signal interval 12 (1-120 Seconds)
		79 - 80 Record Type (35)
*/


		/*	Card Type: 36 (Sign or Pre-timed Signal Control Codes)
		1 -  4 Node number of intersection (1-6999 Node ID)
		6 -  6 Control code for approach link 1, interval 1 (0-9, A)
		7 -  7 Control code for approach link 2, interval 1 (0-9, A)
		8 -  8 Control code for approach link 3, interval 1 (0-9, A)
		9 -  9 Control code for approach link 4, interval 1 (0-9, A)
		10 - 10 Control code for approach link 5, interval 1 (0-9, A)
		11 - 11 Control code for approach link 1, interval 2 (0-9, A)
		12 - 12 Control code for approach link 2, interval 2 (0-9, A)
		13 - 13 Control code for approach link 3, interval 2 (0-9, A)
		14 - 14 Control code for approach link 4, interval 2 (0-9, A)
		15 - 15 Control code for approach link 5, interval 2 (0-9, A)
		16 - 16 Control code for approach link 1, interval 3 (0-9, A)
		17 - 17 Control code for approach link 2, interval 3 (0-9, A)
		18 - 18 Control code for approach link 3, interval 3 (0-9, A)
		19 - 19 Control code for approach link 4, interval 3 (0-9, A)
		20 - 20 Control code for approach link 5, interval 3 (0-9, A)
		21 - 21 Control code for approach link 1, interval 4 (0-9, A)
		22 - 22 Control code for approach link 2, interval 4 (0-9, A)
		23 - 23 Control code for approach link 3, interval 4 (0-9, A)
		24 - 24 Control code for approach link 4, interval 4 (0-9, A)
		25 - 25 Control code for approach link 5, interval 4 (0-9, A)
		26 - 26 Control code for approach link 1, interval 5 (0-9, A)
		27 - 27 Control code for approach link 2, interval 5 (0-9, A)
		28 - 28 Control code for approach link 3, interval 5 (0-9, A)
		29 - 29 Control code for approach link 4, interval 5 (0-9, A)
		30 - 30 Control code for approach link 5, interval 5 (0-9, A)
		31 - 31 Control code for approach link 1, interval 6 (0-9, A)
		32 - 32 Control code for approach link 2, interval 6 (0-9, A)
		33 - 33 Control code for approach link 3, interval 6 (0-9, A)
		34 - 34 Control code for approach link 4, interval 6 (0-9, A)
		35 - 35 Control code for approach link 5, interval 6 (0-9, A)
		36 - 36 Control code for approach link 1, interval 7 (0-9, A)
		37 - 37 Control code for approach link 2, interval 7 (0-9, A)
		38 - 38 Control code for approach link 3, interval 7 (0-9, A)
		39 - 39 Control code for approach link 4, interval 7 (0-9, A)
		40 - 40 Control code for approach link 5, interval 7 (0-9, A)
		41 - 41 Control code for approach link 1, interval 8 (0-9, A)
		42 - 42 Control code for approach link 2, interval 8 (0-9, A)
		43 - 43 Control code for approach link 3, interval 8 (0-9, A)
		44 - 44 Control code for approach link 4, interval 8 (0-9, A)
		45 - 45 Control code for approach link 5, interval 8 (0-9, A)
		46 - 46 Control code for approach link 1, interval 9 (0-9, A)
		47 - 47 Control code for approach link 2, interval 9 (0-9, A)
		48 - 48 Control code for approach link 3, interval 9 (0-9, A)
		49 - 49 Control code for approach link 4, interval 9 (0-9, A)
		50 - 50 Control code for approach link 5, interval 9 (0-9, A)
		51 - 51 Control code for approach link 1, interval 10 (0-9, A)
		52 - 52 Control code for approach link 2, interval 10 (0-9, A)
		53 - 53 Control code for approach link 3, interval 10 (0-9, A)
		54 - 54 Control code for approach link 4, interval 10 (0-9, A)
		55 - 55 Control code for approach link 5, interval 10 (0-9, A)
		56 - 56 Control code for approach link 1, interval 11 (0-9, A)
		57 - 57 Control code for approach link 2, interval 11 (0-9, A)
		58 - 58 Control code for approach link 3, interval 11 (0-9, A)
		59 - 59 Control code for approach link 4, interval 11 (0-9, A)
		60 - 60 Control code for approach link 5, interval 11 (0-9, A)
		61 - 61 Control code for approach link 1, interval 12 (0-9, A)
		62 - 62 Control code for approach link 2, interval 12 (0-9, A)
		63 - 63 Control code for approach link 3, interval 12 (0-9, A)
		64 - 64 Control code for approach link 4, interval 12 (0-9, A)
		65 - 65 Control code for approach link 5, interval 12 (0-9, A)
		77 - 77 External Control Flag (0,2)
		79 - 80 Record Type (36)
		*/

	}
}
void trfGeneration::Type36()
{
	int line1 = 0;
	for (int i = 0; i <total_corsim_nodes.size(); i ++)
	{
		if (total_corsim_nodes[i] > 7999) continue;
		for (int j = 0; j < 80; j ++)	TRF_36.push_back(' ');
		TRF_36.push_back('\n');
		line1++;
	}

	//int pos[12][5]; // 12 intervals, 5 links
	int lines = 0;
	int pos[6];
	pos[0] = 3; pos[1]=5; pos[2]=6; pos[3]=7; pos[4]=8; pos[5]=9; 
	for (int i = 0; i <total_corsim_nodes.size(); i ++)
	{
		if (total_corsim_nodes[i] > 7999) continue;
		for (int j = i; j < vsta_ndlist.size(); j ++)
		{
			if (vsta_ndlist[j].crsm_ID() == total_corsim_nodes[i])
			{
				vector <int> inf;				
				vector <int> upnodes= vsta_ndlist[j].returnUpNodes();
				inf.push_back(vsta_ndlist[j].crsm_ID());
				for (int k = 0; k < upnodes.size(); k++) inf.push_back(upnodes[k]);

				for (int order1 = 0; order1 < inf.size(); order1++)
				{
					char buffer [16];
					if (order1 != 0) inf[order1] = 1;
					sprintf(buffer, "%d",inf[order1]);
					int tens = 0;
					while (float(inf[order1]) / double(pow(10.0,tens))>=10) tens++;
					for (int k = 0; k <= tens; k++) 
					{
						TRF_36[lines*81 + pos[order1]- k]=buffer[tens-k];
					}
				}

				TRF_36[lines*81+78]='3';
				TRF_36[lines*81+79]='6';
				lines++;
				break;
			}
		}
	}

	/*	Card Type: 36 (Sign or Pre-timed Signal Control Codes)
	1 -  4 Node number of intersection (1-6999 Node ID)
	6 -  6 Control code for approach link 1, interval 1 (0-9, A)
	7 -  7 Control code for approach link 2, interval 1 (0-9, A)
	8 -  8 Control code for approach link 3, interval 1 (0-9, A)
	9 -  9 Control code for approach link 4, interval 1 (0-9, A)
	10 - 10 Control code for approach link 5, interval 1 (0-9, A)
	11 - 11 Control code for approach link 1, interval 2 (0-9, A)
	12 - 12 Control code for approach link 2, interval 2 (0-9, A)
	13 - 13 Control code for approach link 3, interval 2 (0-9, A)
	14 - 14 Control code for approach link 4, interval 2 (0-9, A)
	15 - 15 Control code for approach link 5, interval 2 (0-9, A)
	16 - 16 Control code for approach link 1, interval 3 (0-9, A)
	17 - 17 Control code for approach link 2, interval 3 (0-9, A)
	18 - 18 Control code for approach link 3, interval 3 (0-9, A)
	19 - 19 Control code for approach link 4, interval 3 (0-9, A)
	20 - 20 Control code for approach link 5, interval 3 (0-9, A)
	21 - 21 Control code for approach link 1, interval 4 (0-9, A)
	22 - 22 Control code for approach link 2, interval 4 (0-9, A)
	23 - 23 Control code for approach link 3, interval 4 (0-9, A)
	24 - 24 Control code for approach link 4, interval 4 (0-9, A)
	25 - 25 Control code for approach link 5, interval 4 (0-9, A)
	26 - 26 Control code for approach link 1, interval 5 (0-9, A)
	27 - 27 Control code for approach link 2, interval 5 (0-9, A)
	28 - 28 Control code for approach link 3, interval 5 (0-9, A)
	29 - 29 Control code for approach link 4, interval 5 (0-9, A)
	30 - 30 Control code for approach link 5, interval 5 (0-9, A)
	31 - 31 Control code for approach link 1, interval 6 (0-9, A)
	32 - 32 Control code for approach link 2, interval 6 (0-9, A)
	33 - 33 Control code for approach link 3, interval 6 (0-9, A)
	34 - 34 Control code for approach link 4, interval 6 (0-9, A)
	35 - 35 Control code for approach link 5, interval 6 (0-9, A)
	36 - 36 Control code for approach link 1, interval 7 (0-9, A)
	37 - 37 Control code for approach link 2, interval 7 (0-9, A)
	38 - 38 Control code for approach link 3, interval 7 (0-9, A)
	39 - 39 Control code for approach link 4, interval 7 (0-9, A)
	40 - 40 Control code for approach link 5, interval 7 (0-9, A)
	41 - 41 Control code for approach link 1, interval 8 (0-9, A)
	42 - 42 Control code for approach link 2, interval 8 (0-9, A)
	43 - 43 Control code for approach link 3, interval 8 (0-9, A)
	44 - 44 Control code for approach link 4, interval 8 (0-9, A)
	45 - 45 Control code for approach link 5, interval 8 (0-9, A)
	46 - 46 Control code for approach link 1, interval 9 (0-9, A)
	47 - 47 Control code for approach link 2, interval 9 (0-9, A)
	48 - 48 Control code for approach link 3, interval 9 (0-9, A)
	49 - 49 Control code for approach link 4, interval 9 (0-9, A)
	50 - 50 Control code for approach link 5, interval 9 (0-9, A)
	51 - 51 Control code for approach link 1, interval 10 (0-9, A)
	52 - 52 Control code for approach link 2, interval 10 (0-9, A)
	53 - 53 Control code for approach link 3, interval 10 (0-9, A)
	54 - 54 Control code for approach link 4, interval 10 (0-9, A)
	55 - 55 Control code for approach link 5, interval 10 (0-9, A)
	56 - 56 Control code for approach link 1, interval 11 (0-9, A)
	57 - 57 Control code for approach link 2, interval 11 (0-9, A)
	58 - 58 Control code for approach link 3, interval 11 (0-9, A)
	59 - 59 Control code for approach link 4, interval 11 (0-9, A)
	60 - 60 Control code for approach link 5, interval 11 (0-9, A)
	61 - 61 Control code for approach link 1, interval 12 (0-9, A)
	62 - 62 Control code for approach link 2, interval 12 (0-9, A)
	63 - 63 Control code for approach link 3, interval 12 (0-9, A)
	64 - 64 Control code for approach link 4, interval 12 (0-9, A)
	65 - 65 Control code for approach link 5, interval 12 (0-9, A)
	77 - 77 External Control Flag (0,2)
	79 - 80 Record Type (36)
*/

}

void trfGeneration::Type50()
{
	int line1 = 0;
	for (int i = 0; i < vista_links.size(); i ++)
	{
		int corsim_up = vista_links[i][3];
		int corsim_dn = vista_links[i][4];
		if( corsim_up > 5999 || (corsim_dn < 7000 && corsim_dn >=6000 )) //boundary link
		{
			for (int j = 0; j < 80; j ++)	TRF_50.push_back(' ');
			TRF_50.push_back('\n');
			line1++;			
		}
	}
	int lines = 0;

	
	
	for (int order = 0; order < linklist.size();order++ )
	{
		if (linklist[order].returnVistaType() == -1) continue;		

		int upnode = linklist[order].returnCorsimUp();
		int dnnode = linklist[order].returnCorsimDn();
		if (upnode == 8009)
			int abc=123;
		if ( upnode > 7999)
		{
			cout<< "boundary node  50  " << linklist[order].returnCorsimUp()<<endl;
			vector <vector <int>> linkflow = linklist[order].returnLinkFlow();  //0=dn's id, 1 = vehid, 2= time
			int boundary_flow = 0;
			for (int i = 0; i < linkflow.size(); i ++)
			{
				if (linkflow[i][2] > 3600) continue;
				boundary_flow ++;
			}	
			int pos[3];
			pos[0] = 3; pos[1] = 7; pos[2] = 11;

			int inf[3];
			inf[0] = upnode; inf[1] = dnnode; inf[2] = boundary_flow;
			for (int order1 = 0; order1 < (sizeof(pos)/sizeof(*pos)); order1++)
			{
				char buffer [16];
				if(inf[order1] ==0) continue;

				sprintf(buffer, "%d",inf[order1]);
				int tens = 0;
				while (float(inf[order1]) / double(pow(10.0,tens))>=10) tens++;
				for (int k = 0; k <= tens; k++) 
				{
					TRF_50[lines*81 + pos[order1]- k]=buffer[tens-k];
				}
			}
			
			TRF_50[lines*81+11]='0'; //flow rate
			
			
			TRF_50[lines*81+22]='1';
			TRF_50[lines*81+23]='0';
			TRF_50[lines*81+24]='0'; //HOV violator percentage

			TRF_50[lines*81+78]='5';
			TRF_50[lines*81+79]='0';
			lines++;		
		}

	}
	/*
	Card Type: 50 (Entry Link Volumes)
	Columns:
	1 -  4 Upstream node number (i) (8000-8999 Node ID)
	5 -  8 Downstream node number (j) (1-6999 Node ID)
	9 - 12 Flow rate entering from entry node number i on entry link (i,j) (0-9999 Vehicles Per Hour)
	
	21 - 25 HOV violator percentage (0-99999 Hundredths of Percentage)
	79 - 80 Record Type (50)
	*/

}

void trfGeneration::Type51()
{
	int line1 = 0;
	for (int i = 0; i <  vista_links.size(); i ++)
	{
		int corsim_up = vista_links[i][3];
		int corsim_dn = vista_links[i][4];
		if( corsim_up > 5999 || (corsim_dn < 7000 && corsim_dn >=6000 )) //boundary link
		{
			for (int j = 0; j < 80; j ++)	TRF_51.push_back(' ');
			TRF_51.push_back('\n');
			line1++;			
		}
	}

	int lines = 0;
	for (int order = 0; order < linklist.size();order++ )
	{
		if (linklist[order].returnVistaType() == -1) continue;
		if (linklist[order].returnCorsimUp() < 6000 && linklist[order].returnCorsimDn() < 6000 )  continue;//boundary link
		if (linklist[order].returnCorsimDn() > 8000 ) continue;
		
		cout<< "boundary node 51   " << linklist[order].returnCorsimUp()<<endl;
		vector <vector <int>> linkflow = linklist[order].returnLinkFlow();  //0=dn's id, 1 = vehid, 2= time
		int linkid = linklist[order].returnVistaID();

		if (linkid == 318525)
			int abc = 0;

		int boundary_flow = 0;
		for (int i = 0; i < linkflow.size(); i ++)
		{
			if (linkflow[i][2] > 3600) continue;
			boundary_flow ++;
		}			

		int upnode = linklist[order].returnCorsimUp();
		int dnnode = linklist[order].returnCorsimDn();
		if ((linklist[order].returnCorsimUp() < 7000 && linklist[order].returnCorsimUp() >=6000 )|| (linklist[order].returnCorsimDn() < 7000 && linklist[order].returnCorsimDn() >=6000 ))
		{
			int pos[19];
			for (int i = 0; i < (sizeof(pos)/sizeof(*pos)); i ++) pos[i] = 7+i*4;
			int upnode = linklist[order].returnCorsimUp();
			int dnnode = linklist[order].returnCorsimDn();

			int inf[19];
			inf[0] = upnode; inf[1] = dnnode; inf[2] = boundary_flow; 
			for (int i = 3; i < (sizeof(pos)/sizeof(*pos)); i ++) inf[i] = 0;
			
			if (linklist[order].returnCorsimDn() < 7000 && linklist[order].returnCorsimDn() >=6000 ) 
				inf[2] = -boundary_flow;			
			for (int order1 = 0; order1 < (sizeof(pos)/sizeof(*pos)); order1++)
			{
				char buffer [16];

				sprintf(buffer, "%d",inf[order1]);
				int tens = 0;
				while (float(abs(inf[order1])) / double(pow(10.0,tens))>=10) tens++;
				if (inf[order1] < 0) tens++;
				for (int k = 0; k <= tens; k++) 
				{
					TRF_51[lines*81 + pos[order1]- k]=buffer[tens-k];
				}
			}

			TRF_51[lines*81+78]='5';
			TRF_51[lines*81+79]='1';
			lines++;			
		}

	}
	/*
	Card Type: 50 (Entry Link Volumes)
	Columns:
	1 -  4 Upstream node number (i) (8000-8999 Node ID)
	5 -  8 Downstream node number (j) (1-6999 Node ID)
	9 - 12 Flow rate entering from entry node number i on entry link (i,j) (0-9999 Vehicles Per Hour)
	
	21 - 25 HOV violator percentage (0-99999 Hundredths of Percentage)
	79 - 80 Record Type (50)
	*/

}

void trfGeneration::output()
{
	ofstream reviseTRF ("D:\\Amber\\\corsim_test\\revise_test.trf");
	for (int i = 0; i < pTRF.size() -1; i ++)
	{
		reviseTRF<<pTRF[i];
		//cout<<pTRF[i];
	}
	reviseTRF<<"\n\n";
	for (int i = 0; i < TRF_11.size() -1; i ++)
	{
		reviseTRF<<TRF_11[i];
	}
	reviseTRF<<"\n\n";
	for (int i = 0; i < TRF_21.size() -1; i ++)
	{
		reviseTRF<<TRF_21[i];
	}

	reviseTRF<<"\n\n";
	for (int i = 0; i < TRF_35.size() -1; i ++)
	{
		reviseTRF<<TRF_35[i];
	}
	
	reviseTRF<<"\n\n";
	for (int i = 0; i < TRF_36.size() -1; i ++)
	{
		reviseTRF<<TRF_36[i];
	}

	reviseTRF<<"\n\n";
	for (int i = 0; i < TRF_50.size() -1; i ++)
	{
		reviseTRF<<TRF_50[i];
	}

	reviseTRF<<"\n\n";
	for (int i = 0; i < TRF_51.size() -1; i ++)
	{
		reviseTRF<<TRF_51[i];
	}

	reviseTRF<<"\n\n";
	for (int i = 0; i < TRF_170.size() -1; i ++)reviseTRF<<TRF_170[i];

	reviseTRF<<"\n\n";
	for (int i = 0; i < TRF_195.size() -1; i ++)
	{
		reviseTRF<<TRF_195[i];
		//cout<<pTRF[i];
	}
	reviseTRF<<"\n\n";
	for (int i = 0; i < TRF_210.size() -1; i ++)reviseTRF<<TRF_210[i];

}