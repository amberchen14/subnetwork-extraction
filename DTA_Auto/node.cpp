#include "stdafx.h"
#include "node.h"

node ::node()
{
	Signal_ID = 0;
}
node::~node(){}



void node::vista_locate(int id, int type, float x, float y)
{
	vsta_type = type; 
	vsta_node_id = id;
	vsta_node_x = x;
	vsta_node_y = y;
}

float node::vsta_X()
{
	return vsta_node_x;
}

float node::vsta_Y()
{
	return vsta_node_y;
}

int node:: vsta_ID()
{
	return vsta_node_id;
}

int node::returnVstaType()
{
	return vsta_type;
}

void node::corsim_position(int id, float x, float y)
{
	crsm_node_id = id;
	crsm_node_x = int((vsta_node_x-x)*315634.7194176)+1;
	crsm_node_y = int((vsta_node_y-y)*363692.5642944)+1;	

}

void  node ::corsim_node_locate(int id, int x, int y, int flag)
{
	crsm_node_id = id;
	crsm_node_x = x;
	crsm_node_y = y;
}

int node:: crsm_X()
{
	return crsm_node_x;
}

int  node::crsm_Y()
{
	return crsm_node_y;
}

void node::offset(int offsettime)
{
	 OffSet = offsettime;
}

int node::returnoffset()
{
	return OffSet;
}

void node::inputCrsmID(int id )
{
	crsm_node_id = id;
}

int node::crsm_ID()
{
	return crsm_node_id;
}

void node :: UpNodes(vector <int> up)
{
	upnodes = up;
}

vector <int> node:: returnUpNodes()
{
	return upnodes;
}

void node ::newType(int type)
{
	vsta_type = type; 
}


void node:: input(int id, int phase, int nodeid, int timered, int timeyellow, int timegreen, CString cs,CString vs)
{
	Signal_ID = id;
	Phases = phase;
	Red = timered;
	Green = timegreen;
	Yellow = timeyellow;
	LinkFrom = cs;
	LinkTo = vs;
	
	phase_order.push_back(Phases);
	
	vector <int> phasetime;
	phasetime.push_back(Green);
	phasetime.push_back(Yellow);
	phasetime.push_back(Red);
	phase_time.push_back(phasetime);

	InputLinkFrom(LinkFrom);
	InputLinkTo(LinkTo);


}

int node::returnSignalID()
{
	return Signal_ID;
}

void node::inputCorsimNode(int node)
{
	Corsim_node = node;

}



int node::returnCorsimNode()
{
	return Corsim_node;
}


vector <int> node::returnPhaseOrder()
{
	return phase_order;
}

vector <vector <int>> node::returnPhaseTime()
{
	return phase_time;
}

void node::InputLinkFrom(CString & links_string)
{

	std::vector <char> char_linkfrom;
	std::vector <int> int_linkfrom;
	char_linkfrom.resize(links_string.GetLength());
	std::transform(links_string.GetString(),
					links_string.GetString() + links_string.GetLength(),
                   char_linkfrom.begin(),
                    [](TCHAR c) { return static_cast<char>(c); });

	int link_id = 0;
	for (int i = 0; i < char_linkfrom.size(); i++)
	{
		if (char_linkfrom[i] == '{') continue;
		else if (char_linkfrom[i]== ',' || char_linkfrom[i] == '}')
		{	
			int_linkfrom.push_back(link_id);
			link_id = 0;
		}
		else
		{
			link_id = link_id*10+int(char_linkfrom[i]) - '0';
		}
	}

	phase_linkfrom.push_back(int_linkfrom);
}

void node::InputLinkTo(CString & links_string)
{
	std::vector <char> char_linkto;
	std::vector <int> int_linkto;

	char_linkto.resize(links_string.GetLength());
	std::transform(links_string.GetString(),
					links_string.GetString() + links_string.GetLength(),
                    char_linkto.begin(),
                    [](TCHAR c) { return static_cast<char>(c); });

	int link_id = 0;
	for (int i = 0; i < char_linkto.size(); i++)
	{
		if (char_linkto[i] == '{') continue;
		else if (char_linkto[i]== ',' || char_linkto[i] == '}')
		{	
			int_linkto.push_back(link_id);
			link_id = 0;
		}
		else
		{
			link_id = link_id*10+int(char_linkto[i]) - '0';
		}
	}
	phase_linkto.push_back(int_linkto);
}


vector <vector <int>> node::returnLinkFrom() 
{
	return phase_linkfrom;
}

vector <vector <int>>  node::returnLinkTo()
{
	return phase_linkto;
}

void node:: InputNodeFrom(vector<int> nodes)
{
	node_from.push_back( nodes);

}

void node::RecordPhaseColor( vector<vector<int>> phasecolor)
{
	 PhaseColor = phasecolor;	
}



vector <vector <int>> node::  returnNodeFrom()
{
	return node_from;
}

vector <vector <int>> node::  returnNodeTo()
{
		return node_to;
}

void node::InputNodeTo(vector<int> nodes)
{
	node_to.push_back( nodes);
}

void node::RecordPhaseColor(vector<vector<int>> uplinks, vector<vector<int>> phasecolor)
{
	 UniqueUplinks = uplinks;
	 PhaseColor = phasecolor;	
}
vector<vector<int>> node:: returnUniqueUplinks()
{
	return  UniqueUplinks;
}
vector<vector<int>> node::returnPhaseColor()
{
	return PhaseColor;
}






