#include "stdafx.h"
#include "vista.h"

int link_id;
int link_org;
int link_dest;

vista ::vista(){}
vista::~vista(){}


void vista::details(int id, int org, int dest, int flag)
{
	link_id = id;
	link_org = org;
	link_dest = dest;
	link_flag = flag;
}

int vista::ID()
{
	return link_id;
}

int vista::ORG()
{
	return link_org;
}

int vista::DEST()
{
	return link_dest;
}

void vista::downstream(int id, int dest)
{
	link_id = id;
	link_dest = dest;

}

int vista::downnode()
{
	return link_dest;
}

int vista::downlk()
{
	return link_id;
}

int vista :: FLAG()
{
	return link_flag;
}

void vista::node_locate(int id, float x, float y)
{
	node_id = id;
	node_x = x;
	node_y = y;
}

int  vista:: node_ID()
{
	return node_id;
}

float  vista::node_X()
{
	return node_x;
}

float vista::node_Y()
{
	return node_y;
}


void vista::CSVT_node(int crsm, int vsta, int flag1)
{
	node_crsm = crsm;
	node_vsta =vsta;
	node_flag  = flag1;
}

int vista::NODE_CRSM()
{
	return node_crsm;
}

int vista::NODE_VSTA()
{
	return node_vsta;
}

int vista::NODE_FLAG()
{
	return node_flag;
}
